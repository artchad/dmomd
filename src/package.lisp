;;;; package.lisp

(defpackage #:dmomd
  (:local-nicknames (:tgk #:trivial-gamekit)
                    (:a #:alexandria)
                    (:dungen #:mfiano.graphics.procgen.dungen))
  (:use #:cl)
  (:export #:start
           #:stop
           #:restart))
