(in-package #:dmomd)

(defvar +sqrt-2+ 1.414213562)

(defun square (num)
  (* num num))

(defun within-circle-p (radius x-offset y-offset &optional (compare-func '>=))
  "Checks weather a point with an offset of (`x-offset'/`y-offset')
 from the center of the circle,
 lays within the circle defines by `radus'."
  (funcall compare-func radius (sqrt (+ (square x-offset) (square y-offset)))))

(defun determinant-2x2 (a b c d)
  "`arr' is a one-dimensional array with 4 elements."
  (- (* a d)
     (* b c)))

(defun num-between-p (num b1 b2)
  "doc"
  (and (> num b1) (< num b2)))

(defun negative-p (num)
  (> 0 num) )

(defun positive-p (num)
  (<= 0 num) )

(defun tangential (cell-pos r other-cell-pos)
  (or (equalp (seq+ other-cell-pos (vector   0    r)) cell-pos)
      (equalp (seq+ other-cell-pos (vector   r    0)) cell-pos)
      (equalp (seq+ other-cell-pos (vector   0  (- r))) cell-pos)
      (equalp (seq+ other-cell-pos (vector (- r)  0)) cell-pos)))
