(in-package #:dmomd)


;;;; CREDITS MODE
;;;;----------------------------------------------------------------------------
(defclass credits-mode (mode) ()
  (:default-initargs
   :mode-music nil))


(defmethod bind-buttons ((m credits-mode))
  (setf (bound-buttons m)
        (list :up :down :escape))
  (with-bind-buttons  ()
    ((:down :pressed)
     (decf (tgk:y *loose-credits-draw-pos*) 30))
    ((:down :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (decf (tgk:y *loose-credits-draw-pos*) 30)))

    ((:up :pressed)
     (incf (tgk:y *loose-credits-draw-pos*) 30))
    ((:up :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (incf (tgk:y *loose-credits-draw-pos*) 30)))

    ((:escape :pressed) (switch-mode 'title-mode))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init ((m credits-mode))
  (if *player-won*
      (let ((credits-duration 35)
            (fade-duration 3))
        (progn
          (add-timer (+ (now) (- *dungeon-leave-animation-duration* 1))
                     (lambda () (add-fade :duration 1 :ease :in)))
          (add-timer (+ (now) *dungeon-leave-animation-duration*)
                     (lambda () (add-fade :duration 1 :ease :out)))
          (draw-for (*dungeon-leave-animation-duration* :win-credits-background)
            `(draw-image *window-bottom-left-corner*
                         :img-credits-background-1))
          (add-animation *animation-credits-player-won*)

          (add-timer (+ (now) credits-duration)
                     (lambda () (switch-mode 'title-mode)))
          (add-timer (+ (now) (- credits-duration fade-duration))
                     (lambda () (add-fade :duration fade-duration :ease :in)))))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m credits-mode)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m credits-mode))
  (if *player-won*
      (draw-win-credits)
      (draw-loose-credits)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m credits-mode)))
