;;;; rogue-mode-utils.lisp
(in-package #:dmomd)

;;;;-----------------------------------------------------------------------------
(defun move/rel! (to)
  (unless *in-battle*
    ;; set correct animation
    (cond
      ((equalp to #( 0  1))  ; north
       (setf (current-animation *player*) *animation-dungeon-player-north*))
      ((equalp to #( 0 -1))  ; south
       (setf (current-animation *player*) *animation-dungeon-player-south*))
      ((equalp to #( 1  0))  ; east
       (setf (current-animation *player*) *animation-dungeon-player-east*))
      ((equalp to #(-1  0))  ; west
       (setf (current-animation *player*) *animation-dungeon-player-west*)))
    ;; update player position
   (teleport-to! (new-position/rel (player-pos) to))))

(defun retreat! ()
  ;; *last-safe-cell* is is the previous-cell, period.
  (setf (dungeon-pos *player*) *last-safe-cell*))

(defmethod teleport-to! (to)
  (unless (equalp to (player-pos)) ; to prevent playing walking sound, when standing still
    (unless *in-battle*
      (play-sound (a:random-elt '(:snd-walk-1 :snd-walk-2 :snd-walk-3 :snd-walk-4)))
      (setf (dungeon-pos *player*) to))))

(defmethod teleport-to! :after (to)
  (update-last-safe-cell!)
  (update-view to)
  (unless (check-for-battle! *player* *enemies*)
    (pickup-items *rogue-mode-items*)))

(defun update-last-safe-cell! ()
  (setf *last-safe-cell* (player-pos)))

(defun update-view (to)
  (let ((visible-cells (visible-cells)))
    (setf *seen-cells* (union visible-cells *seen-cells* :test #'equalp))
    (setf *currently-visible-cells* visible-cells)
    (setf (room-id *player*) (cell-region-id (grid-seq-ref to *grid*)))))

;;;;;------------------------------------------------------------------------------
(defun visible-cells (&optional (player *player*))
  "Calculates the cells the player can see
Returns a list of cells"
  ;; perform line of sight calculations
  (calc-los-visible-area)
  ;; TEMPORARY STUB just return everything
  ;; (player-visible-cells)
  )

(defun ready-to-repeat-p ()
  (< *button-repeat-delay* (- (now) *dirty-last-repeat*)))

(defun rogue-draw-tiles ()
  "Draws the map tiles in rogue-mode.
   We start with the bottom-left corner cell (might be slightly outside of the screen),
   and draw enough to cover the screen."
  (for-cells-in-view-of (*dungeon-camera* x y curr-cell-pos)
    (cond ((cell-visible-p curr-cell-pos)
           (draw-dungeon-cell-sprite curr-cell-pos))
          ;; if we reach this case that means the cell is currently not visible, but was already seen
          ((cell-seen-p curr-cell-pos)
           (ge.vg:with-alpha (.3)
             (draw-dungeon-cell-sprite  curr-cell-pos)))
          ;; don't draw anything, when the tile was not yet seen
          (T nil))))

(defun rogue-draw-items (item-list)
  (dolist (item item-list)
    (when (and (member (dungeon-pos item) *currently-visible-cells* :test #'equalp)
               (or (eql (dungen::cell-region (grid-seq-ref (player-pos) *grid*))
                        (dungen::cell-region (grid-seq-ref (player-pos) *grid*)))
                   (stands-on-door-p *player*)))
      (draw-item item))))


(defun rogue-draw-enemies (enemy-list)
  (dolist (enemy enemy-list)
    (when (member (dungeon-pos enemy) *currently-visible-cells* :test #'equalp)
      (when *show-aggression-tiles*
        (draw-aggression-mask enemy))
      (draw enemy
            (vector->vec2 (position-inside-camera
                           *dungeon-camera*
                           (cell-position->pixel/bottom-left (dungeon-pos enemy))))))))

(defun rogue-draw-debug-info ()
  (when *draw-poly-edges*
    (draw-poly-edges *edge-pool*))
  (when *show-player-vision-radius*
    (draw-player-vision-radius))
  (when *show-regions*
    (show-regions *grid*))
  ;; overlay
  (draw-debug-overlay))

(defun move-player (direction)
  (ecase direction
    (:up    (move/rel! #(0 1)))
    (:down  (move/rel! #(0 -1)))
    (:left  (move/rel! #(-1 0)))
    (:right (move/rel! #(1 0))))
  (setf *dirty-last-repeat* (now)))

(defun check-for-battle! (player enemies)
  "Returns true if the player is within the aggro range of an enemy.
   Also sets *in-battle* to T if in battle."
  (let ((safe-p t))
    ;; check all enemies on the map for if the player is in their aggression range
    (let ((battle-enemies (list)))
      (dolist (enemy enemies (not safe-p)) ; return safe-p when in range of enemy
        (when (and (player-in-enemy-view-p enemy player)
                   (not *peaceful*))
          (setf safe-p nil)
          ;; populate the battle-enemies list with the encountered enemies
          (push enemy battle-enemies)
          ;; wait a bit before switching
          (unless *in-battle*
            (setf *in-battle* t)
            ;; fade to black when entering a battle
            (add-fade :color +color-black+ :duration *battle-enter-delay* :ease :in)
            (play-sound :snd-gasp)
            (add-timer (+ *battle-enter-delay* (now))
                       (lambda ()
                         (setf *battle-enemies* battle-enemies)
                         ;; enemies need to be deleted later on
                         (switch-mode 'battle-mode)))))))))

(defun pickup-items (items)
  (dolist (item (remove-if-not
                 (lambda (item) (equalp (player-pos) (dungeon-pos item)))
                 items))
    (pick-up item)))

(defun bind-inventory-buttons (rogue-mode)
  (setf (bound-buttons rogue-mode)
        (list :h :j :k :l :e :escape :up :down :left :right :enter :tab))
  (with-bind-buttons ()
    ((:e :pressed) (toggle-inventory))
    ((:escape :pressed) (toggle-inventory))

    ((:k    :pressed)
     (next-menu-item (active-menu rogue-mode)))
    ((:up    :pressed)
     (next-menu-item (active-menu rogue-mode)))

    ((:j  :pressed)
     (previous-menu-item (active-menu rogue-mode)))
    ((:down  :pressed)
     (previous-menu-item (active-menu rogue-mode)))

    ((:h  :pressed)
     (select-menu-previous rogue-mode))
    ((:left  :pressed)
     (select-menu-previous rogue-mode))

    ((:l :pressed)
     (menu-call-current-menu-item (active-menu rogue-mode))
     (select-menu-next rogue-mode))
    ((:right :pressed)
     (menu-call-current-menu-item (active-menu rogue-mode))
     (select-menu-next rogue-mode))
    ((:enter :pressed)
     (menu-call-current-menu-item (active-menu rogue-mode))
     (select-menu-next rogue-mode))

    ((:tab :pressed)
     (print "Opened rogue-mode-menu.")
     (setf *show-player-information-menu*
           (not *show-player-information-menu*)))
    ((:tab :released)
     (print "Closed rogue-mode-menu.")
     (setf *show-player-information-menu*
           (not *show-player-information-menu*)))))

(defun toggle-inventory ()
  (print "Toggled Inventory menu")
  (play-sound :snd-button-change)
  (setf (list-menu-index *rogue-mode-inventory-menu*) 0)
  (when *show-inventory-menu* ; we're leaving the inventory menu
    (bind-buttons *mode*)
    (setf (root-menu *mode*)
          (setf (active-menu *mode*) nil)))
  (unless *show-inventory-menu*  ; we're entering the inventory menu
    (unbind-buttons *mode*)
    (bind-inventory-buttons *mode*)
    (setf (root-menu *mode*)
          (setf (active-menu *mode*) *rogue-mode-inventory-menu*)))
  (setf *show-inventory-menu*
        (not *show-inventory-menu*)))

(defun draw-inventory-menu ()
  (with-accessors  ((menu root-menu))
      *mode*
    (draw-menu menu)
    (with-accessors ((w menu-width)
                     (h menu-height)
                     (o menu-origin)
                     (fs menu-font-size)
                     (bgc menu-background-color))
        menu
      (let ((draw-pos (tgk:add (tgk:vec2 20 (- h 40)) o))
            (c (hexcolor "#EDAC3F")))
        (draw-string-list draw-pos
                          fs
                          c
                          (font :quikhand fs)
                          fs
                          (list "Inventory:"))))))

(defun create-inventory-menu ()
  (let ((font-size 30)
        (menu-color (alpha-color .7 (hexcolor "#1C1411"))))
    (let ((inventory-menu
            (setf *rogue-mode-inventory-menu*
                  (make-list-menu
                      '(:health-potion :mana-potion :exit)
                      '("Health Potion" "Mana Potions" "Exit")
                      (list (lambda ()
                              (if (has-item :health-potion *player*)
                                  (if (not (full-health-p *player*))
                                      (progn
                                        (decf (gethash :health-potion (inventory *player*)))
                                        (play-sound (item-sound (gethash :health-potion *items*)))
                                        (funcall (item-effect (gethash :health-potion *items*))))
                                      (draw-for (1 :full-health-notification)
                                        `(draw-text "I don't have any injuries."
                                                    ,(tgk:add *window-center* (tgk:vec2 -590 0))
                                                    :fill-color ,+color-red1+
                                                    :font ,(font :quikhand 40))))
                                  (draw-for (1 :no-health-potion-notification)
                                    `(draw-text "I don't have any health potions.."
                                                ,(tgk:add *window-center* (tgk:vec2 -590 0))
                                                :fill-color ,+color-red1+
                                                :font ,(font :quikhand 40)))))
                            (lambda ()
                              (if  (has-item :mana-potion *player*)
                                   (if (not (full-mana-p *player*))
                                       (progn (decf (gethash :mana-potion (inventory *player*)))
                                              (play-sound (item-sound (gethash :mana-potion *items*)))
                                              (funcall (item-effect (gethash :mana-potion *items*))))
                                       (draw-for (1 :full-mana-notification)
                                         `(draw-text "I can't store more Mana."
                                                     ,(tgk:add *window-center* (tgk:vec2 -590 0))
                                                     :fill-color ,+color-blue1+
                                                     :font ,(font :quikhand 40))))
                                   (draw-for (1 :no-mana-potion-notification)
                                     `(draw-text "I don't have any more Mana potions."
                                                 ,(tgk:add *window-center* (tgk:vec2 -590 0))
                                                 :fill-color ,+color-blue1+
                                                 :font ,(font :quikhand 40)))))
                            (lambda () (toggle-inventory)))
                    :origin (tgk:vec2 40 40)
                    :width 200
                    :height 300
                    :font-size font-size
                    :drawing-offset (tgk:vec2 0 (- (* 2 font-size)))
                    :background-color menu-color
                    :visibility :active))))
      (setf (menu-item-child-menu (get-menu-item :health-potion inventory-menu))
            (make-instance 'info-menu
                           :name :health-potion-infor
                           :text (list "Restores a bit of your life.")
                           :origin (tgk:vec2 260 40)
                           :width 200
                           :height 300
                           :font-size 20
                           :parent-menu inventory-menu
                           :background-color menu-color
                           :visibility :active))

      (setf (menu-item-child-menu (get-menu-item :mana-potion inventory-menu))
            (make-instance 'info-menu
                           :name :mana-potions-info
                           :text (list "Restores a bit of your mana.")
                           :origin (tgk:vec2 260 40)
                           :width 200
                           :height 300
                           :font-size 20
                           :parent-menu inventory-menu
                           :background-color menu-color
                           :visibility :active)))))

(defclass fade ()
  ((start-time :initarg :start-time :reader start-time)
   (end-time   :initarg :end-time   :reader end-time)
   (duration   :initarg :duration   :reader duration)
   (ease       :initarg :ease       :reader ease
               :documentation "A keyword, either `:in' or `:out'.")
   (color      :initarg :color      :accessor color))
  (:documentation "An object representing a fading effect."))

(defun make-fade (start-time end-time ease color)
  (make-instance 'fade
                 :start-time start-time
                 :end-time end-time
                 :duration (- end-time start-time)
                 :ease ease
                 :color (cond ((eq ease :in)  (alpha-color 0 color))
                              ((eq ease :out) (alpha-color 1 color))
                              (t (error "fade ease needs to be either :in or :out. Got ~A" ease)))))

(defun add-fade (&key (color +color-black+) (duration 1) (ease :in))
  "`ease' can have the values `:in' and `:out'.
The opacity value in `color' doesn't change this functions behavior."
  (push (make-fade (now) (+ duration (now)) ease color) *fade-list*))

(defun process-fades ()
  (dolist (f *fade-list*)
    (update-fade-color f)
    (if (time-ran-out (+ (end-time f) 0.02))
        (setf *fade-list* (delete f *fade-list*))
        (draw-fade f))))

(defun update-fade-color (f)
  (let ((ease (ease f)))
   (cond ((eq ease :in)  (%update-fade-color-in  f))
         ((eq ease :out) (%update-fade-color-out f))
         (t (error "fade ease needs to be either :in or :out. Got ~A" ease)))))

(defun %update-fade-color-in (f)
  (with-accessors ((duration duration)
                   (c color))
      f
    (symbol-macrolet ((a (tgk:w c)))
      (incf a (/ 1 (* 60 duration))))))

(defun %update-fade-color-out (f)
  (with-accessors ((duration duration)
                   (c color))
      f
    (symbol-macrolet ((a (tgk:w c)))
      (decf a (/ 1 (* 60 duration))))))


(defmethod draw-fade ((f fade))
  (gamekit:draw-rect *window-bottom-left-corner*
                     *window-width*
                     *window-height*
                     :fill-paint (color f)))

(defun kill-all-enemies-and-spawn-key ()
  (setf *enemies* nil)
  (spawn-hellfire-key (player-pos)))


(defun no-rats-left-p ()
  (dolist (e *enemies*)
    (when (eq (type-of e) 'rat)
      (return-from no-rats-left-p nil)))
  t)
