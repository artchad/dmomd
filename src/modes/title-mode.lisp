(in-package #:dmomd)


;;;-----------------------------------------------------------------------------
;;; Initializes the entire game
;;;-----------------------------------------------------------------------------

(defun initialize-game ()
  (setf *player-won* nil)
  (setf *dungeon-rooms* (make-hash-table :test #'equalp))  ; used by make-dungeon
  (setf *dungeon-empty-rooms* nil)                         ; used by make-dungeon
  (make-dungen)                                            ; make-dungeon
  (setf *dungeon-camera* (make-camera 0
                                      0
                                      (elt *internal-screen-size* 0)
                                      (elt *internal-screen-size* 1)))
  (setf *rogue-mode-items* nil)
  (setf *rogue-mode-inventory-menu* nil)
  (setf *settings-menu* nil)
  (setf *items* (make-hash-table))
  (create-reference-items *items*)

  (populate-poly-edges *grid*)  ; necessary for the LOS algorithm
  (setf *seen-cells* nil)
  (setf *last-safe-cell* nil)
  (setf *timers* nil)
  (setf *in-battle* nil)
  (setf *player-won* nil)
  (setf *enemies* nil)
  (create-all-animations)
  ;; player should be created here
  (setf *player*
        (make-instance 'player
                       :hp-max 70
                       :mp-max 8
                       :vision-radius 3.45
                       :vulnerabilities (list :poison)
                       :resistances (list :fire :lightning)))

  (setf *demon* (make-demon))
  ;; The order in which these spawn functions are called matters a lot!!!
  (spawn-player)
  (spawn-treasure)
  (dotimes (i 2)
      (spawn-enemies))
  ;; spawn potions
  (dotimes (i 3)
    (spawn-items)))

;;;; TITLE MODE
;;;;----------------------------------------------------------------------------
(defclass title-mode (mode) ()
  (:default-initargs
   :mode-music :snd-title-mode-music))

(defmethod bind-buttons ((m title-mode))
  (setf (bound-buttons m)
        (list :h :j :k :l :down :up :right :enter))
  (with-bind-buttons ()
    ((:j   :pressed)  (previous-menu-item (active-menu m)))
    ((:down   :pressed)  (previous-menu-item (active-menu m)))
    ((:k     :pressed)  (next-menu-item     (active-menu m)))
    ((:up     :pressed)  (next-menu-item     (active-menu m)))
    ((:l  :pressed)  (menu-call-current-menu-item (active-menu m)))
    ((:enter  :pressed)  (menu-call-current-menu-item (active-menu m)))
    ((:right  :pressed)  (menu-call-current-menu-item (active-menu m)))
    ((:escape :pressed)  (toggle-settings-menu))))

(defmethod create-interface ((m title-mode))
  (create-settings-menu)
  (create-controls-menu)
  (setf (root-menu m)
        (setf (active-menu m)
              (make-list-menu
                  (list :game :settings :controls :credits :quit)
                  '("Game" "Settings" "Controls" "Credits" "Quit")
                  (list (lambda () (format t "starting... rogue~%")
                              (initialize-game)
                              (switch-mode 'rogue-mode))
                        (lambda () (toggle-settings-menu))
                        (lambda () (toggle-controls-menu))
                        (lambda () (switch-mode 'credits-mode))
                        (lambda () (format t "quitting...~%")
                              (tgk:stop)))
                :name "title-menu"
                :origin (tgk:vec2 20 20)
                :width 340
                :height 280
                :font-size 60
                :visibility :active)))
  (setf (drawing-offset (root-menu m)) (tgk:vec2 0 50)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init :before ((m title-mode))
  (setf *timers* nil)
  (setf *drawing-queue* nil)
  (setf *animation-queue* nil)
  (setf *fade-list* nil)
  (setf *win-credits-draw-pos* (tgk:vec2 550 200))
  (setf *loose-credits-draw-pos* (tgk:vec2 400 640))
  (setf *player-won* nil)
  (setf *in-boss-battle-p* nil)
  (setf *treasure* nil)
  (setf *dont-bind-buttons* nil))

(defmethod mode-init ((m title-mode)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m title-mode))
  ;; this makes sure the title music is played as soon as it's loaded
  (when *play-mode-music*
   (unless (member :snd-title-mode-music *playing-sounds*)
     (play-sound :snd-title-mode-music :looped-p t)
     (push :snd-title-mode-music *playing-sounds*))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m title-mode))
  (draw-image (tgk:vec2 0 0) :img-splash)
  (draw-menu (root-menu m)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m title-mode))
  (setf *show-settings-menu* nil)
  (setf *show-controls-menu* nil))
