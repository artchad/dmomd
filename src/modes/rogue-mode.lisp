(in-package #:dmomd)


;;;;-----------------------------------------------------------------------------
;;;; ROGUE MODE
;;;;-----------------------------------------------------------------------------
(defclass rogue-mode (mode) ()
  (:default-initargs
   :mode-music :snd-rogue-mode-music)
  (:documentation "This is the mode you enter, once you've aggro-ed an enemy."))

(defmethod create-interface ((m rogue-mode))
  (create-inventory-menu)
  (create-settings-menu))

(defmethod bind-buttons ((m rogue-mode))
  (setf (bound-buttons m)
        (list :h :j :k :l :down :up :left :right :e :tab :escape :f1 :f2 :f3 :f4 :f5 :f6 :f7 :f8))
  (with-bind-buttons  ()
    ((:j :pressed)
     (move-player :down))
    ((:j :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :down)))
    ((:down :pressed)
     (move-player :down))
    ((:down :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :down)))

    ((:k :pressed)
     (move-player :up))
    ((:k :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :up)))
    ((:up :pressed)
     (move-player :up))
    ((:up :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :up)))

    ((:h :pressed)
     (move-player :left))
    ((:h :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :left)))
    ((:left :pressed)
     (move-player :left))
    ((:left :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :left)))

    ((:l :pressed)
     (move-player :right))
    ((:l :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :right)))
    ((:right :pressed)
     (move-player :right))
    ((:right :repeating)
     (when (ready-to-repeat-p)
       (setf *dirty-last-repeat* (now))
       (move-player :right)))

    ((:tab :pressed)
     (print "Opened rogue-mode-menu.")
     (setf *show-player-information-menu*
           (not *show-player-information-menu*)))
    ((:tab :released)
     (print "Closed rogue-mode-menu.")
     (setf *show-player-information-menu*
           (not *show-player-information-menu*)))
    ((:e :pressed) (toggle-inventory))
    ((:escape :pressed) (toggle-settings-menu))
    ((:f1 :pressed) (setf *debug-mode* (not *debug-mode*)))
    ((:f2 :pressed) (setf *peaceful* (not *peaceful*)))
    ((:f3 :pressed) (setf *draw-poly-edges* (not *draw-poly-edges*)))
    ((:f4 :pressed) (create-edge-colors *curr-edge-id*))
    ((:f5 :pressed) (setf *show-regions* (not *show-regions*)))
    ((:f6 :pressed) (setf *show-aggression-tiles* (not *show-aggression-tiles*)))
    ((:f7 :pressed) (setf *player-invincible* (not *player-invincible*)))
    ((:f8 :pressed) (setf *player-can-not-kill-enemy* (not *player-can-not-kill-enemy*)))
    ((:f9 :pressed) (setf *show-player-vision-radius* (not *show-player-vision-radius*)))
    ((:f9 :released) (setf *show-player-vision-radius* (not *show-player-vision-radius*)))
    ((:f10 :pressed) (setf *show-grid-p* (not *show-grid-p*)))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init ((m rogue-mode))
  (pickup-items *rogue-mode-items*)
  (setf (current-animation *player*) *animation-dungeon-player-idle*))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m rogue-mode))
  (slide-camera-towards! *dungeon-camera* (player-pos)))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m rogue-mode))
  (unless *in-boss-battle-p*
   (rogue-draw-tiles)
   (rogue-draw-items *rogue-mode-items*)
   (rogue-draw-enemies *enemies*)
   (draw *player* (vector->vec2 (position-inside-camera
                                 *dungeon-camera*
                                 (cell-position->pixel/bottom-left (dungeon-pos *player*)))))
   (draw-player-bars)
   (draw-image :img-top-down-sprite-sheet
               *window-center*)

   (when *show-player-information-menu*
     (draw-player-information-menu))
   (when *show-inventory-menu*
     (draw-inventory-menu))
   (when *show-settings-menu*
     (draw-settings-menu))
   ;; display additional information to help with debugging
   (when *debug-mode*
     (rogue-draw-debug-info))
   (when *show-grid-p*
     (draw-grid (gray-shade :value 1 :opacity .1))
     (draw-text (format nil "(~A/~A)" (elt (player-pos) 0) (elt (player-pos) 1))
                (tgk:subt *window-center* (tgk:vec2 20 43))
                :fill-color (tgk:vec4 1 1 1 1)
                :font (font :quikhand 20)))))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m rogue-mode))
  (setf *timers* nil)
  (setf *show-inventory-menu* nil)
  (setf *show-settings-menu* nil))
