(in-package #:dmomd)


(defclass loading-mode (mode) ()
  (:default-initargs
   :mode-music nil))


(defmethod mode-init ((m loading-mode))
  )


(defmethod mode-act ((m loading-mode))
  (incf *icon-rotation* .03))

(defmethod mode-draw ((m loading-mode))
  (let ((col (hexcolor "#851818")))
    (draw-text "Loading"
               (tgk:vec2 30 50)
               :fill-color col)
    (tgk:with-pushed-canvas ()
      (tgk:translate-canvas 150 55)
      (gamekit:rotate-canvas *icon-rotation*)
      (gamekit:draw-polygon (list (tgk:vec2 -30 0)
                                  (tgk:vec2 0 30)
                                  (tgk:vec2 30 0)
                                  (tgk:vec2 0 -30)
                                  (tgk:vec2 -30 0))
                            :fill-paint (hexcolor "#851818")))))

(defmethod mode-cleanup ((m loading-mode)))
