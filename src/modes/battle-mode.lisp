(in-package #:dmomd)


;;;;-----------------------------------------------------------------------------
(defclass battle-mode (mode)
  ((enemies
    :accessor battle-mode-enemies
    :initarg :enemies
    :initform nil))
  (:default-initargs
   :mode-music :snd-battle-mode-music))

(defmethod create-interface ((m battle-mode))
  "Create menu and add to menus and set as active-menu."
  (with-accessors ((active-menu active-menu)
                   (menus-to-draw battle-mode-menus-to-draw)
                   (root-menu root-menu))
      m
    (let ((menu-color (gamekit:vec4 .08 .05 .04 1)))
      (setf root-menu
            (setf active-menu ; set this to the active menu
                  (make-list-menu
                      ;; those are the names, the children can use
                      (list :attacks  :items  :run)
                      (list "Attacks" "Items" "Run") ; "run" will be the bottom most item
                      (list
                       (lambda ())
                       (lambda ())
                       (lambda ()
                         (progn
                           (push (list *player* nil :player-run) *attacker-queue*)
                           (setf *current-attacker* (fastest-fighter *battle-enemies*)))))
                    :origin (tgk:vec2 10 0)
                    :width  180
                    :height 180
                    :background-color menu-color
                    :name :root-menu
                    :background-color (tgk:vec4 1 1 1 .15)
                    :visibility :active)))
      ;; attack menu
      (let ((attack-menu
             (setf (menu-item-child-menu (get-menu-item :attacks root-menu))
                   (make-list-menu
                       (attack-keywords *player*)
                       (if *debug-mode*
                           (list "slash" "fireball" "lightning" "deny-existence")
                           (list "slash" "fireball" "lightning"))
                       (if *debug-mode*
                        (list
                         (player-create-attack-callback :attack-slash)
                         (player-create-attack-callback :attack-fireball)
                         (player-create-attack-callback :attack-lightning)
                         (player-create-attack-callback :attack-deny-existence))
                        (list
                         (player-create-attack-callback :attack-slash)
                         (player-create-attack-callback :attack-fireball)
                         (player-create-attack-callback :attack-lightning)))
                     :origin (tgk:vec2 200 0)
                     :width 220
                     :height 180
                     :background-color menu-color
                     :name :attack-menu
                     :parent-menu root-menu
                     :background-color (tgk:vec4 1 1 1 .15)
                     :visibility :inactive))))
        ;; create info menus for the attacks
        (let ((info-font-size 30)
              (atk-slash          (get-attack :attack-slash          *player*))
              (atk-fireball       (get-attack :attack-fireball       *player*))
              (atk-lightning      (get-attack :attack-lightning      *player*))
              (atk-deny-existence (get-attack :attack-deny-existence *player*)))
         (setf (menu-item-child-menu (get-menu-item :attack-slash attack-menu))
               (make-instance 'info-menu
                              :name :slash-info
                              :text (list (format nil "dmg: ~A"
                                                  (attack-damage atk-slash))
                                          (format nil "type: ~A"
                                                  (string-capitalize (string-downcase (attack-type atk-slash))))
                                          (format nil "Manacost: ~A" (attack-manacost atk-slash)))
                              :origin (tgk:vec2 430 0)
                              :width 220
                              :height 180
                              :font-size info-font-size
                              :parent-menu attack-menu
                              :background-color menu-color
                              :visibility :active))
         (setf (menu-item-child-menu (get-menu-item :attack-fireball attack-menu))
               (make-instance 'info-menu
                              :name :fireball-info
                              :text (list (format nil "dmg: ~A "
                                                  (attack-damage atk-fireball))
                                          (format nil "type: ~A"
                                                  (string-capitalize (string-downcase (attack-type atk-fireball))))
                                          (format nil "Manacost: ~A" (attack-manacost atk-fireball)))
                              :origin (tgk:vec2 430 0)
                              :width 220
                              :height 180
                              :font-size info-font-size
                              :parent-menu attack-menu
                              :background-color menu-color
                              :visibility :active))
         (setf (menu-item-child-menu (get-menu-item :attack-lightning attack-menu))
               (make-instance 'info-menu
                              :name :lightning-info
                              :text (list (format nil "dmg: ~A "
                                                  (attack-damage atk-lightning))
                                          (format nil "type: ~A"
                                                  (string-capitalize
                                                   (string-downcase
                                                    (attack-type atk-lightning))))
                                          (format nil "Manacost: ~A" (attack-manacost atk-lightning)))
                              :origin (tgk:vec2 430 0)
                              :width 220
                              :height 180
                              :font-size info-font-size
                              :parent-menu attack-menu
                              :background-color menu-color
                              :visibility :active))
         (when *debug-mode*
          (setf (menu-item-child-menu (get-menu-item :attack-deny-existence attack-menu))
                (make-instance 'info-menu
                               :name :deny-existence-info
                               :text (list "way too much"
                                           (format nil "type: ~A"
                                                   (string-capitalize (string-downcase (attack-type atk-deny-existence))))
                                           (format nil "Manacost: ~A" (attack-manacost atk-deny-existence)))
                               :origin (tgk:vec2 430 0)
                               :width 220
                               :height 180
                               :font-size info-font-size
                               :parent-menu attack-menu
                               :background-color menu-color
                               :visibility :active)))))
      ;; inventory menu
      (let ((items-menu
             (setf (menu-item-child-menu (get-menu-item :items root-menu))
                   (make-list-menu
                       (list :health-potion :mana-potion)
                       (list "health-potion" "mana-potion")
                       (list (lambda ()
                               (when (and (eq *current-attacker* *player*)
                                          (not (null (gethash :health-potion (inventory *player*))))
                                          (< 0 (gethash :health-potion (inventory *player*))))
                                 (push (list *player* nil :health-potion) *attacker-queue*)
                                 (setf *current-attacker* (fastest-fighter *battle-enemies*))))
                             (lambda ()
                               (when (and (eq *current-attacker* *player*)
                                          (not (null (gethash :mana-potion (inventory *player*))))
                                          (< 0 (gethash :mana-potion (inventory *player*))))
                                 (push (list *player* nil :mana-potion) *attacker-queue*)
                                 (setf *current-attacker* (fastest-fighter *battle-enemies*)))))
                     :origin (tgk:vec2 200 0)
                     :width 220
                     :height 180
                     :background-color menu-color
                     :name :items-menu
                     :parent-menu root-menu
                     :background-color (tgk:vec4 1 1 1 .15)
                     :visibility :inactive))))
        (setf (menu-item-child-menu (get-menu-item :health-potion items-menu))
              (make-instance 'info-menu
                             :name :health-potion-infor
                             :text (list "Restores a bit of your life.")
                             :origin (tgk:vec2 430 0)
                             :width 220
                             :height 180
                             :font-size 20
                             :parent-menu items-menu
                             :background-color menu-color
                             :visibility :active))
        (setf (menu-item-child-menu (get-menu-item :mana-potion items-menu))
              (make-instance 'info-menu
                             :name :mana-potions-info
                             :text (list "Restores a bit of your mana.")
                             :origin (tgk:vec2 430 0)
                             :width 220
                             :height 180
                             :font-size 20
                             :parent-menu items-menu
                             :background-color menu-color
                             :visibility :active))))))

(defmethod bind-buttons ((m battle-mode))
  (setf (bound-buttons m )
        (list :h :j :k :l :up :down :left :right :enter :tab :escape))
    (with-bind-buttons ()
      ((:j :pressed)
       (play-sound :snd-button-change)
       (previous-menu-item (active-menu m)))
      ((:down :pressed)
       (play-sound :snd-button-change)
       (previous-menu-item (active-menu m)))
      ((:k :pressed)
       (play-sound :snd-button-change)
       (next-menu-item (active-menu m)))
      ((:up :pressed)
       (play-sound :snd-button-change)
       (next-menu-item (active-menu m)))
      ((:h :pressed)
       (play-sound :snd-button-change)
       (select-menu-previous m))
      ((:left :pressed)
       (play-sound :snd-button-change)
       (select-menu-previous m))
      ((:l :pressed)
       (when (and (battle-going-on-p *player* (battle-mode-enemies m))
                  (eq *current-attacker* *player*))
         (menu-call-current-menu-item (active-menu m)))
       (select-menu-next m))
      ((:right :pressed)
       (when (and (battle-going-on-p *player* (battle-mode-enemies m))
                  (eq *current-attacker* *player*))
         (menu-call-current-menu-item (active-menu m)))
       (select-menu-next m))
      ((:enter :pressed)
       (when (and (battle-going-on-p *player* (battle-mode-enemies m))
                  (eq *current-attacker* *player*))
         (menu-call-current-menu-item (active-menu m)))
       (select-menu-next m))
      ((:tab :pressed)
       (print "Opened rogue-mode-menu.")
       (setf *show-player-information-menu*
             (not *show-player-information-menu*)))
      ((:tab :released)
       (print "Closed rogue-mode-menu.")
       (setf *show-player-information-menu*
             (not *show-player-information-menu*)))
      ((:escape :pressed)
       (toggle-settings-menu))
      ((:f7 :pressed)
       (setf *player-invincible* (not *player-invincible*)))
      ((:f8 :pressed)
       (setf *player-can-not-kill-enemy* (not *player-can-not-kill-enemy*)))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-init :before ((m battle-mode))
  (print "~&Entered Battle-Mode!~%~%")
  (setf *in-battle* t)
  (if *in-boss-battle-p*
      (setf (mode-music m) :snd-boss-battle-music)
      (setf (mode-music m) :snd-battle-mode-music))
  (when *in-boss-battle-p*
    (add-timer (+ (now) (- *boss-battle-enter-delay* 1.2))
               (lambda () (play-sound :snd-demon-laugh)))
    (add-timer (+ (now) (1- *boss-battle-enter-delay*))
               (lambda () (add-fade :duration 1 :ease :in)))
    (add-timer (+ (now) *boss-battle-enter-delay*)
               (lambda () (add-fade :duration .5 :ease :out)))))

(defmethod mode-init ((m battle-mode))
  (when *in-boss-battle-p*
    (let ((dt *boss-battle-enter-delay*))
      (draw-for (dt :first-person-hands)
        `(draw-image *hands-draw-pos*
                     :img-first-person-hands))
      (draw-for (dt :first-person-treasure)
        `(draw-image *window-bottom-left-corner*
                     :img-first-person-chest))))

  (setf (battle-mode-enemies m) *battle-enemies*)
  (setf *enemies-that-have-not-attacked* *battle-enemies*)
  (setf *current-attacker* *player*)
  (setf *hands-draw-pos* (tgk:vec2 0 -200))

  (setf (current-animation *player*) *animation-battle-player-idle*)
  (set-enemy-battle-animations (battle-mode-enemies m))
  (set-enemy-battle-positions (battle-mode-enemies m) 100 -80))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m battle-mode))
  (delete-dead-enemies)
  (resolve-attack-queue)
  (switch-mode-if-ready)

  (when (and *in-boss-battle-p*
             (mode-younger-than-p (1- *boss-battle-enter-delay*)))
    (setf *hands-draw-pos* (tgk:vec2 (tgk:x *hands-draw-pos*)
                                     (+ (tgk:y *hands-draw-pos*) .5)))))

;;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m battle-mode))
  ;; draw background
  (draw-image *window-bottom-left-corner* :img-battle-background-1)
  (draw *player*)
  (draw-battle-enemies)
  (draw-effects *effect-animations*)
  ;; HUD and UI drawing
  (draw-player-bars)
  (draw-menu (root-menu m))
  (when *show-player-information-menu*
    (draw-player-information-menu)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m battle-mode))
  (stop-sounds)
  (add-fade :color +color-black+ :duration .5 :ease :out)
  (setf (has-attacked-p *player*) nil)
  (setf (current-animation *player*)
        (get-animation :idle (dungeon-animations *player*)))
  (clean-attributes *battle-enemies*)
  (reset-enemy-battle-positions *battle-enemies*) ; set them back to where they were before
  (setf *attacker-queue* nil)
  (setf *battle-enemies* nil)
  ;; order of the two following lines is important
  (setf *in-battle* nil))
