(in-package #:dmomd)

(defclass can-fight ()
  ((battle-animations :accessor battle-animations :initarg :battle-animations)
   (attaking-p        :accessor attacking-p       :initarg :attacking-p       :initform nil)
   (has-attacked-p    :accessor has-attacked-p    :initarg :has-attacked-p    :initform nil)
   (last-attack-time  :accessor last-attack-time  :initarg :last-attack-time  :initform 0)
   (attack-speed      :accessor attack-speed      :initarg :attack-speed      :initform 0.6
                      :documentation "Should be a bit longer than the fastest animation.")
   (dead-p            :accessor dead-p            :initarg :dead-p            :initform nil)
   (hp                :accessor hp                :initarg :hp                :initform 15)
   (hp-max            :accessor hp-max            :initarg :hp-max            :initform 15)
   (mp                :accessor mp                :initarg :mp                :initform 10)
   (mp-max            :accessor mp-max            :initarg :mp-max            :initform 10)
   (xp                :accessor xp                :initarg :xp                :initform 0
                      :documentation "The amount of xp within the current level.")
   (xp-threshold      :accessor xp-threshold      :initarg :xp-threshold      :initform 10
                      :documentation "The threshold that needs to be passed in order to level up.")
   (xp-total          :accessor xp-total          :initarg :xp-total          :initform 0)
   (level             :accessor level             :initarg :level             :initform 0)
   (status-effects    :accessor status-effects    :initarg :status-effects    :initform nil
                      :documentation "A list of keywords, like '(paralysis sleep)")
   (agility           :accessor agility           :initarg :agility           :initform 3)
   (strength          :accessor strength          :initarg :strength          :initform 1)
   (intelligence      :accessor intelligence      :initarg :intelligence      :initform 1)
   (physical-defense  :accessor physical-defense  :initarg :physical-defense  :initform 1)
   (initiative        :accessor initiative        :initarg :initiative        :initform 1)
   (attacks           :accessor attacks           :initarg :attacks           :initform nil)
   (vision-radius     :accessor vision-radius     :initarg :vision-radius     :initform nil
                      :allocation :class)
   (vision-mask       :accessor vision-mask       :initarg :vision-mask       :initform nil
                      :allocation :class)

   (vulnerabilities
    :accessor vulnerabilities
    :initarg :vulnerabilities
    :initform nil
    :documentation "A list of keywords.")
   (resistances
    :accessor resistances
    :initarg :resistances
    :initform nil
    :documentation "A list of keywords.")
   (status-resistances
    :accessor status-resistances
    :initarg :status-resistances
    :initform nil
    :documentation "A list of keywords.")
   (room-id
    :accessor room-id
    :initarg :room-id
    :initform -1
    :documentation "Every can-fight needs a current room-id."))

  (:documentation "This is a class, which defines the necessary slots for a fighter."))


(defmethod attack ((attacker can-fight) (attacked can-fight) atk-keyword)
  ;; currently the players animation needs to be changed,
  ;; because attacking-p is only set to false once the current animation is finished
  ;; The idle animation is looping and therefore never finishes
  "`attacker' performs the attack defined by `atk-keyword' against `attacked'.
Sleep, paralysis, etc. was already taken into account.
Set `attacker's new mp based on the attacks mana cost and
set `attacked'  new hp based on the attacks damage."
  (format t "~&~A (~A/~A) attacked ~A (~A/~A) with ~A."
          (type-of attacker)
          (hp attacker)
          (hp-max attacker)
          (type-of attacked)
          (hp attacked)
          (hp-max attacked)
          (symbol-name atk-keyword))
  (let* ((attacker-new-mp (calc-new-mp-of attacker atk-keyword))
         (dmg (calc-dmg attacked attacker atk-keyword)))
    (setf (mp attacker) attacker-new-mp)
    (damage-for dmg attacked))
  (when (>= 0 (hp attacked))
    ;; the attacked one was the player and the player died
    (unless (and (equalp attacked *player*)
                 *player-invincible*)
      (setf (current-animation attacked) (get-animation :dying (battle-animations attacked)))
      (start-animation (current-animation attacked) (now))
      (setf (dead-p attacked) t)
      (unless *in-boss-battle-p*
        (add-xp attacker attacked)))))


(defmethod attack :after ((attacker can-fight) (attacked can-fight) atk-keyword)
  (format t "attack :after called with ~A." attacker)
  ;; start hurt animation
  (setf (last-attack-time attacker) (now))
  (unless (dead-p attacked)
    (setf (current-animation attacked) (get-animation :hurt (battle-animations attacked)))
    (start-animation (current-animation attacked) (now)))
  (setf (attacking-p attacker) t)
  (setf (has-attacked-p attacker) t))


(defmethod get-attack (atk-keyword (cf can-fight))
  (cdr (assoc atk-keyword (attacks cf))))

(defmethod can-attack-p ((cf can-fight) atk-keyword)
  (and (enough-mana-p cf atk-keyword)
       (not (dead-p cf))))

(defmethod enough-mana-p ((cf can-fight) atk-keyword)
  (let* ((atk (get-attack atk-keyword cf))
         (manacost (attack-manacost atk))
         (new-mp (- (mp cf) manacost)))
    (cond ((zerop manacost) t)
          ((< new-mp 0) nil)
          (t t))))

(defmethod attack-keywords (cf)
  "Returns a list of the names of all the players attacks."
  (mapcar #'car (attacks cf)))

(defmethod attack-names (cf)
  "Returns a list of the names of all the players attacks."
  (mapcar (lambda (e) (attack-name (cdr e))) (attacks cf)))

(defun calc-new-hp-of (attacked attacker atk-keyword)
  "Returns an integer indicating the remaining hp of `attacked'."
  (let ((dmg (calc-dmg attacked attacker atk-keyword)))
    (format t "(DMG: ~A)~%" dmg)
    (if (eq dmg :infinity)
        0 ; instant kill
        (a:clamp  (- (hp attacked) dmg) 0 (hp-max attacked)))))

(defun calc-dmg (attacked attacker atk-keyword)
  "Calculates the damage of the attack based on the stats of the `attacker' and the `attacked'.
   Returns :infinity if damage is infinity."
  (let ((unmodified-dmg (calc-unmodified-dmg attacked attacker atk-keyword)))
    (if (eq unmodified-dmg :infinity)
        unmodified-dmg
        (if (crit? (agility attacker))
            (progn
              (format t "~&Critical hit for ~A dmg.~%" (* 2 unmodified-dmg))
              (draw-for (.4 :dmg-number-crit)
                `(draw-text ,(princ-to-string (* 2 unmodified-dmg))
                            ,(tgk:add (battle-draw-pos attacked) (tgk:vec2 100 120))
                            :fill-color ,+color-red+
                            :font ,(font :quikhand 110)))
              (draw-for (.4 :dmg-number-crit)
                `(draw-text ,(princ-to-string (* 2 unmodified-dmg))
                            ,(tgk:add (battle-draw-pos attacked) (tgk:vec2 100 120))
                            :fill-color ,+color-black+
                            :font ,(font :quikhand 120)))
              ;; return damage
              (* 2 unmodified-dmg))
            ;;----------------------------------------------------------
            (progn
              (draw-for (.4 :dmg-number-normal)
                `(draw-text ,(princ-to-string unmodified-dmg)
                            ,(tgk:add (battle-draw-pos attacked) (tgk:vec2 100 120))
                            :fill-color ,+color-red+
                            :font ,(font :quikhand 80)))
              (draw-for (.4 :dmg-number-normal)
                `(draw-text ,(princ-to-string unmodified-dmg)
                            ,(tgk:add (battle-draw-pos attacked) (tgk:vec2 100 120))
                            :fill-color ,+color-black+
                            :font ,(font :quikhand 90)))

              ;; return damage
              unmodified-dmg)))))

(defun calc-unmodified-dmg (attacked attacker atk-keyword)
  "Returns an integer wich indicates how much damge the base attack does.
   (Without modifiers like critical strikes or other abnormal modifiers.
   Returns infinity if dmg is :infinity."
  (let* ((atk (gethash atk-keyword *attacks*))
         (atk-type (attack-type atk))
         (dmg (%calc-unmodified-dmg attacked attacker atk-keyword)))
    (cond ((eq dmg :infinity) dmg)
          ((member atk-type (vulnerabilities attacked))
           (ceiling (* dmg 2)))
          ((member atk-type (resistances attacked))
           (ceiling (/ dmg 2)))
          (t dmg))))

(defun %calc-unmodified-dmg (attacked attacker atk-keyword)
  "Calculates the damage number. Returns :infinity if damage is infinity."
  (let* ((atk (gethash atk-keyword *attacks*))
         (atk-type (attack-type atk))
         (atk-dmg  (attack-damage atk)))
    (cond ((eq atk-dmg :infinity) atk-dmg)
          ((member atk-type '(:physical :poison))
           (a:clamp (ceiling (- (+ atk-dmg (strength attacker))
                              (/ (* 6 (log (physical-defense attacked))) 2)))
                  ;; minimun damage
                  1
                  ;; maximum damage
                  (hp attacked)))
          (t ; this means the atacker is using a magical attack and not a phycial
           (+ atk-dmg (intelligence attacker))))))

(defun calc-new-mp-of (attacker atk-keyword)
  (let* ((atk (get-attack atk-keyword attacker))
         (manacost (attack-manacost atk))
         (new-mp (- (mp attacker) manacost)))
    (if (> 0 new-mp)
        (progn (format t "Not enough mana")
               0)
        new-mp)))


(defmethod add-xp ((attacker can-fight) (attacked can-fight))
  "increased the players `xp' and `total-xp' based on the xp of the killed enemy `attacked'."
  (with-accessors ((xp xp)
                   (xp-threshold xp-threshold)
                   (xp-total xp-total))
      attacker
    (let* ((xp-gain (ceiling (/ (xp-total attacked) 10)))
           (xp-to-next-lvl (- xp-threshold xp))
           (how-many-lvl-ups 0)
           (xp-gain-left xp-gain))
      (incf xp-total xp-gain)
      ;; level up player and adjust xp and xp-threshold
      (do () ((zerop xp-gain-left) how-many-lvl-ups)
        (let ((xp-gain-for-this-lvl (a:clamp xp-gain-left 0 xp-to-next-lvl)))
          (incf xp xp-gain-for-this-lvl)
          (decf xp-gain-left xp-gain-for-this-lvl)
          (when (= xp xp-threshold)
            (incf how-many-lvl-ups)
            (incf xp-threshold 10)
            (setf xp-to-next-lvl xp-threshold)
            (setf xp 0))))
      (unless (zerop how-many-lvl-ups)
       (level-up attacker :how-many-lvls how-many-lvl-ups)))))


(defmethod add-xp :after ((attacker can-fight) (attacked can-fight))
  (with-accessors ((xp xp)
                   (xp-threshold xp-threshold)
                   (xp-total xp-total)
                   (level level))
      attacker
    (when (>= xp xp-threshold)
      (level-up attacker))))

(defmethod level-up ((attacker can-fight) &key (how-many-lvls 1))
  nil)

(defmethod level-up :before ((attacker can-fight)  &key (how-many-lvls 1))
  ;; this function is just for logging to the console.
  (with-accessors ((xp xp)
                   (xp-threshold xp-threshold)
                   (xp-total xp-total)
                   (level level))
      attacker
    (format t "~&~A reached level ~A. (~A xp)" attacker level xp-total)))

(defmethod ready-to-attack-p ((cf can-fight))
  (> (now) (+ (last-attack-time cf) (attack-speed cf))))

(defmethod full-health-p ((cf can-fight))
  (= (hp cf) (hp-max cf)))

(defmethod full-mana-p ((cf can-fight))
  (= (mp cf) (mp-max cf)))

(defmethod draw ((cf can-fight) &optional (draw-pos *window-bottom-left-corner*))
  (render-animation (current-animation cf)
                    cf
                    draw-pos))

(defmethod wait ((cf can-fight))
  (setf (has-attacked-p cf) t)
  (setf *attacker-queue* (rest *attacker-queue*)))


(defmethod damage-for (amount (attacked can-fight))
  (if (eq :infinity amount)
      (setf (hp attacked) 0)
      (setf (hp attacked) (a:clamp (- (hp attacked) amount) 0 (- (hp attacked) amount)))))
