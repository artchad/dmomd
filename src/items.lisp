(in-package #:dmomd)

;;;; ITEM
;;;;----------------------------------------------------------------------------
(defclass item (positionable)
  ((image
    :allocation :class
    :accessor item-image
    :initarg :image)
   (attributes
    :accessor item-attributes
    :initarg :attributes
    :initform nil)
   (effect
    :accessor item-effect
    :initarg :effect
    :initform (lambda ()))
   (sound
    :accessor item-sound
    :initarg :sound
    :initform nil)))

(defun play-item-sound (item)
  (and (keywordp (item-sound item)) (play-sound (item-sound item))))

(defun pick-up (item)
  "add item to player inventory, play its cound effect, call its effect function"
  (unless (null item)
    (funcall (item-effect item))
    (format t "~&picked up ~S~%" item)

    ;; when the item is the treasure, we don't want to put it in the players inventory
    (when (eq (type-of item) 'treasure)
      (return-from pick-up))

    (setf *rogue-mode-items* (delete item *rogue-mode-items*))

    (let (value
          exists-p)
      (setf (values value exists-p) (gethash (intern (symbol-name (type-of item)) :keyword) (inventory *player*)))



      (play-sound :snd-item-pickup)
      (play-item-sound item)
      (when exists-p
        (incf (gethash (intern (symbol-name (type-of item)) :keyword) (inventory *player*)))
        (setf (gethash (intern (symbol-name (type-of item)) :keyword) (inventory *player*)) 1)))))


;;;; potions
;;;;-----------------------------------------------------------------------------
(defclass potion (item)
  ((effect
    :accessor potion-effect
    :initarg :effect
    :initform (lambda () nil))))

(defclass mana-potion (potion)
  ((strength
    :accessor mana-potion-strenth
    :initarg :strength
    :initform 5)
   (image :initform (get-resource '(:img :dungeon :potion-mana)))
   (sound :initform :snd-mana-potion)))

(defclass health-potion (potion)
  ((strength
    :accessor health-potion-strenth
    :initarg :strength
    :initform 5)
   (image :initform (get-resource '(:img :dungeon :potion-health)))
   (sound :initform :snd-health-potion)))

(defclass treasure (item)
  ()
  (:documentation "The winning item. If collected he player wins."))

(defclass key (item)
  ()
  (:documentation "A key used for opening doors."))

(defclass hellfire-key (key)
  ((image :initform (get-resource '(:img :dungeon :hellfire-key))))
  (:default-initargs
   :sound :snd-hellfire-key)
  (:documentation "The key used to open the Final room of the dungeon."))

(defun make-hellfire-key (&key dungeon-pos)
  (if (not (and (arrayp dungeon-pos)
                (= (length dungeon-pos) 2)))
      (error "dungeon-pos has to be a 2D-array like #(0 0), but got ~A." dungeon-pos)
      (make-instance 'hellfire-key
                     :dungeon-pos dungeon-pos
                     :effect (lambda ()
                               (update-vision-mask *player* (a:clamp (- (vision-radius *player*) 1)
                                                                     0
                                                                     (- (vision-radius *player*) 1)))
                               (draw-for (2.6 :hellfire-key-collected-text)
                                 `(gamekit:draw-text
                                   "What a strange key I've just picked up."
                                   ,(tgk:subt *window-center* (tgk:vec2 250 80))
                                   :fill-color ,(hexcolor "#ff0000")
                                   :font ,(font :quikhand 45)))))))

(defun make-chest (pos)
  (make-instance 'treasure
                 :effect (lambda ()
                           (add-fade :duration *battle-enter-delay* :ease :in)
                           (add-timer (+ (now) *battle-enter-delay*)
                                      (lambda ()
                                        (setf *in-boss-battle-p* t)
                                        (setf *battle-enemies* (list *demon*))
                                        (switch-mode 'battle-mode))))
                 :dungeon-pos pos
                 :attributes (list :chest)
                 :image (get-resource '(:img :dungeon :chest-opened-full))
                 :sound :snd-chest))

(defun spawn-items ()
  (dolist (room (rest *dungeon-empty-rooms*))  ; because player is spawned in the first one
    (with-percent-chance-of (40)
      (if (chance-% 50) ; 50% chance to be a health potion (otherwise a mana potion)
          (push (make-instance 'health-potion
                               :dungeon-pos (a:random-elt (gethash room *dungeon-rooms*)))
                *rogue-mode-items*)
          (push (make-instance 'mana-potion
                               :dungeon-pos (a:random-elt (gethash room *dungeon-rooms*)))
                *rogue-mode-items*)))))

(defun chest-p (item)
  (member :chest (item-attributes item)))

(defun spawn-hellfire-key (pos)
  "spawns a key next to the player"
  (push (make-hellfire-key :dungeon-pos (random-adjacent-spawn-pos pos))
        *rogue-mode-items*))

(defun adjacent-positions (pos)
  "Returns all adjacent positions, which aren't the position of a wall."
  (let* ((px (elt pos 0))
         (py (elt pos 1))
         (x-min (1- px))
         (x-max (1+ px))
         (y-min (1- py))
         (y-max (1+ py))
         (possible-spawn-positions (list)))
    (loop :for y :from y-min :to y-max :do
         (if (or (= y y-min) (= y y-max))
             (loop :for x :from x-min :to x-max
                :do (push (vector x y) possible-spawn-positions))
             ;; else
             (dolist (x (list x-min x-max))
               (push (vector x y) possible-spawn-positions))))
    possible-spawn-positions))

(defun adjacent-spawn-positions (pos &key blocked-cells)
  (let* ((all-possible-positions (set-difference
                                  (remove-doors
                                   (remove-walls (adjacent-positions pos)))
                                  blocked-cells)))
    (if all-possible-positions
        all-possible-positions
        (error "There are no possible spawn positions adjacent to ~A." pos))))

(defun random-adjacent-pos (pos)
  (a:random-elt (adjacent-positions pos)))

(defun random-adjacent-spawn-pos (pos)
  (a:random-elt (remove-doors (remove-walls (adjacent-positions pos)))))

(defun remove-walls (positions)
  (remove-if #'wall-at-p positions))

(defun remove-doors (positions)
  (remove-if #'door-at-p positions))
