(in-package #:dmomd)


;;;;-----------------------------------------------------------------------------
;;;; MASK
;;;;-----------------------------------------------------------------------------
(defclass mask ()
  ((center :initarg :center :accessor mask-center)
   (radius :initarg :radius :accessor mask-radius)
   (array  :initarg :array  :accessor mask-array)))

(defmethod make-mask ((c can-fight))
  (let* ((r (vision-radius c))
         (r-rounded (round r)))
    (make-circular-vision-mask
     (make-instance 'mask
                    :radius r
                    :center (vector r-rounded r-rounded)
                    :array (make-array (list (1+ (* 2 r))
                                             (1+ (* 2 r)))
                                       :initial-element 0)))))

(defmethod make-mask ((radius number))
  (let* ((r radius)
        (r-rounded (round r)))
    (make-circular-vision-mask
     (make-instance 'mask
                    :radius r
                    :center (vector r-rounded r-rounded)
                    :array (make-array (list (1+ (* 2 r-rounded))
                                             (1+ (* 2 r-rounded)))
                                       :initial-element 0)))))

(defun make-mask-from-array (mask-arr)
  "`mask-arr' is a square array filled with zeroes and ones."
  (let* ((width (array-dimension mask-arr 0))
         (center (vector (ceiling (/ width 2)) (ceiling (/ width 2)))))
    (make-circular-vision-mask
     (make-instance 'mask
                    :radius -1
                    :center center
                    :array mask-arr))))

(defun make-circular-vision-mask (mask)
  (with-accessors ((a mask-array)
                   (c mask-center)
                   (r mask-radius))
      mask
    (2d-loop-over (a x y)
      (if (point-in-vision-radius-p (- x (elt c 0)) (- y (elt c 1)) r)
          (setf (aref a x y) 1)
          (setf (aref a x y) 0))))
  mask)

(defun point-in-vision-radius-p (x y radius)
  (within-circle-p radius x y '>))

(defmethod mask-to-relative (mask)
  "Takes a mask and transforms entries into coordinates relative to the center of the mask."
  (mask-to-relative% (slot-value mask 'array)
                     (slot-value mask 'center)))

;;; we could specify the center inside the mask, but then what is the value
;;; of that cell? (gethash table 'c) ; => (3 3)
(defun mask-to-relative% (mask center)
  "Takes a MASK, which is a vector of vectors.
Extracts 1's and return a list of their positions relative to CENTER."
  (let (positions)
    (loop :for y :from 0 :below (array-dimension mask 0) :do
         (loop :for x :from 0 :below (array-dimension mask 1) :do
              (unless (zerop (aref mask x y))
                (push (vector (- (aref center 0) x)
                              (- (aref center 1) y))
                      positions))))
    positions))


(defun relatives-to-absolutes (origin rs)
  "Transforms relative position matrix to absolute/global position matrix with `origin' as the center."
  (mapcar (lambda (xy)
            (seq+ xy origin))
          rs))
