(in-package #:dmomd)

;;;-----------------------------------------------------------------------------
;;; Resources
;;;-----------------------------------------------------------------------------
(defparameter *resources* nil)
(defparameter *resource-table* (make-hash-table :test #'equalp))
(defparameter *battle-ss-tile-size* 72)

(defparameter *top-down-ss-tile-size* 64)


;;; Window parameters
;;;-----------------------------------------------------------------------------
(defparameter *scale-factor* 1)
(defparameter *scale-vector* (vector *scale-factor* *scale-factor*))

(defparameter *dungeon-cell-width* 64)
(defparameter *dungeon-cell-height* 64)
(defparameter *dungeon-cell-size* (vector *dungeon-cell-width* *dungeon-cell-height*))

(defun change-cell-size! (positive-int)
  (declaim
   (when (not (positive-p positive-int))
     (error "~S is not a positive integer." positive-int))
   (setf *dungeon-cell-width* positive-int)
   (setf *dungeon-cell-height* positive-int)
   (setf *dungeon-cell-size* (vector *dungeon-cell-width* *dungeon-cell-height*))))


(defparameter *window-width*  1280)
(defparameter *window-height* 720)
(defparameter *window-size* (list *window-width* *window-height*))
(defvar *window-center* (tgk:vec2 (/ *window-width* 2)
                                  (/ *window-height* 2)))
(defvar *window-top-left-corner*     (tgk:vec2 0 *window-height*))
(defvar *window-top-right-corner*    (tgk:vec2 *window-width* *window-height*))
(defvar *window-bottom-left-corner*  (tgk:vec2 0 0))
(defvar *window-bottom-right-corner* (tgk:vec2 *window-width* 0))


(defparameter *internal-screen-width* *window-width*)
(defparameter *internal-screen-height* *window-height*)
(defparameter *internal-screen-size* (vector *internal-screen-width*
                                             *internal-screen-height*))

(defparameter *dungeon-camera* nil)  ;  the camera, which defines what we see

;;; Debug Globals
;;;-----------------------------------------------------------------------------
(defvar *debug-mode* nil)
(defparameter *show-grid-p* nil)
(defparameter *draw-poly-edges* nil)
(defparameter *show-regions* nil)
(defparameter *peaceful* nil)

;;; Cell access shorthands
;;;-----------------------------------------------------------------------------
(defvar *north* 0)
(defvar *south* 1)
(defvar *east*  2)
(defvar *west*  3)

;; Other global variables
;;;-----------------------------------------------------------------------------
(defvar *mode* nil)
(defparameter *player* nil)
(defparameter *demon* nil)
(defparameter *position* nil)
(defparameter *enemies* nil)
(defparameter *battle-enemies* nil)
(defparameter *rogue-mode-items* nil)
(defparameter *playing-sounds* nil)
(defparameter *items* (make-hash-table))
(defparameter *timers* nil)
(defvar *dont-bind-buttons* nil)

(defvar +zerop-pos+ (tgk:vec2 0 0))



(defparameter *dungeon-width* 21)
(defparameter *dungeon-height* 21)
(defparameter *dungeon* nil)  ; dungen:stage
(defvar *grid* nil)  ; dungen:stage-grid
(defparameter *dungeon-rooms* nil)
(defparameter *dungeon-empty-rooms* nil)  ; a list of room IDs

(defparameter *line-of-sight-radius* 4)
(defparameter *seen-cells* nil)
(defparameter *currently-visible-cells* nil)
(defvar *edge-pool* (make-hash-table))
(defvar *visible-cells* nil) ; a list of global tile coordinates
(defvar *drawing-queue* nil
  "A list of (cons endtime instructions) containing instructions
to be processed until endtime is reached.")
(defvar *animation-queue* nil
  "A list of animations to be processed")

(defvar *attacks* (make-hash-table)
  "A hash-tablle containing all attacks. The key is the attack keyword.
Don't modify this variable manually. It's used internally to make attacks work.")
(defvar *play-mode-music* t
  "A boolean, which indicates if the mode music should be played or not.")
(defvar *settings-menu* nil)
(defvar *controls-menu* nil)
(defvar *loading-resources* nil)
(defvar *loaded-resources* nil)
(defvar *fade-list* nil)
(defvar *in-boss-battle-p* nil)


;;;-----------------------------------------------------------------------------
;;; Loading-mode
;;;-----------------------------------------------------------------------------
(defvar *number-of-registered-resources* 0)


;;;-----------------------------------------------------------------------------
;;; Battle-mode
;;;-----------------------------------------------------------------------------
(defparameter *battle-player-default-pos* (tgk:vec2 100 200)
  "The general position where the player is drawn in battle-mode.")
(defparameter *battle-enemy-default-pos* (tgk:vec2 800 200)
  "The general position where the enemies are drawn in battle-mode.")

(defparameter *player-health-bar-position* #(100 600))
(defparameter *player-magic-bar-position* #(100 575))

(defparameter *battle-enter-delay* 1)
(defparameter *in-battle* nil)
(defparameter *current-attacker* nil
  "The current `can-fight' to add to the `*attacker-queue*'.")

(defparameter *player-invincible* nil)
(defparameter *effect-animations* nil)  ; a list of animations
(defparameter *attacker-queue* nil)  ; a list of pairs containing the atk-keyword and the attacker
(defparameter *enemies-that-have-not-attacked* nil)

(defparameter *enemy-death-anim-duration* 1.5)

;; for battle mode transition
(defparameter *boss-battle-enter-delay* 5)
(defparameter *hands-draw-pos* (tgk:vec2 0 -200))
;;;-----------------------------------------------------------------------------
;;; Rogue Mode
;;;-----------------------------------------------------------------------------
(defparameter *dirty-last-repeat* 0)
(defparameter *button-repeat-delay* 0.1)

;; battle-mode will set *position* to this on a successful `run'.
(defparameter *last-safe-cell* nil)  ; where to run to.

(defparameter *door-features* (list :door/horizontal :door/vertical))
(defparameter *show-aggression-tiles* t)

(defparameter *last-sleep-time* nil)
(defparameter *player-won* nil)

(defparameter *show-player-information-menu* nil)

(defvar *show-inventory-menu* nil)
(defvar *rogue-mode-inventory-menu* nil)

(defvar *show-settings-menu* nil)
(defvar *show-controls-menu* nil)

(defvar *boss-room-region* nil)
(defvar *show-player-vision-radius* nil)


;;;-----------------------------------------------------------------------------
;;; Rogue Mode
;;;-----------------------------------------------------------------------------
(defvar *player-can-not-kill-enemy* nil)
(defvar *treasure* nil)


;;;-----------------------------------------------------------------------------
;;; Credits Mode
;;;-----------------------------------------------------------------------------
(defparameter *win-credits-draw-pos* (tgk:vec2 500 200))
(defparameter *loose-credits-draw-pos* (tgk:vec2 400 640))
(defparameter *dungeon-leave-animation-duration* 3.7)

;;;-----------------------------------------------------------------------------
;;; Loading Mode
;;;-----------------------------------------------------------------------------
(defparameter *icon-rotation* 0)
