(in-package #:dmomd)


;;;; CAMERA
;;;;----------------------------------------------------------------------------

(defclass camera ()
  ((x :accessor camera-bottom-left-x :initarg :x)
   (y :accessor camera-bottom-left-y :initarg :y)
   (width :accessor camera-width :initarg :width)
   (height :accessor camera-height :initarg :height)))

(defun make-camera (x y width height)
  (make-instance 'camera :x x :y y :width width :height height))

(defmethod camera-size (c)
  "Return the width and height of the camera as a 2D vector."
  (vector (camera-width c) (camera-height c)))

(defmethod camera-bottom-left (c)
  "Returns the cameras bottom left corner as a 2D vector."
  (vector (camera-bottom-left-x c) (camera-bottom-left-y c)))

(defmethod (setf camera-bottom-left) (val place)
  "Changes the cameras bottom left corner to `val'.
`val' needs to be a sequence with two elements."
  (setf (camera-bottom-left-x place) (elt val 0))
  (setf (camera-bottom-left-y place) (elt val 1))
  (camera-bottom-left place))

(defmethod camera-center (c)
  "Returns the camera center as a 2D vector relative to
the bottom left corner of the camera."
  (seq+ (seq/ (camera-size c) '(2 2))
        (camera-bottom-left c)))

(defmethod (setf camera-center) (val place)
  "Changes the cameras bottom left corner to fit the new center.
Returns the new center."
  (setf (camera-bottom-left place)
        (seq- val
              (seq/ (camera-size place) '(2 2))))
  (camera-center place))

(defmethod position-inside-camera (c p)
  "From absolute position to position relative to (inside) the camera
(relative to the bottom left corner of the camera).
The returned position is in pixels.
`p' is a sequence"
  (seq- p (camera-bottom-left c)))

(defun slide-camera-towards! (camera position)
  (let* ((pos (cell-position->pixel/center position))
         (x (elt pos 0))
         (y (elt pos 1)))
    (setf (camera-center camera)
          (vector (a:lerp .05 (elt (camera-center camera) 0) x)
                  (a:lerp .05 (elt (camera-center camera) 1) y)))))

(defun camera-tile-resolution (&optional (camera *dungeon-camera*) (cell-size *dungeon-cell-size*))
  "Returns the resolution measured in tiles."
  (seq/ (camera-size camera) cell-size))

(defmacro for-cells-in-view-of ((camera x-sym y-sym cell-pos-sym) &body body)
  "You can use x-sym, y-sym and cell-pos-sym."
  (a:with-gensyms (origin-cell camera-tile-resolution)
    `(let* ((,origin-cell (pixel-position->cell-position (camera-bottom-left ,camera)))
            (,camera-tile-resolution (camera-tile-resolution ,camera)))
       ;; we start with the bottom-left corner cell (might be slightly outside of view
       ;; and draw enough to cover the screen.
       (loop :for ,x-sym :from (elt ,origin-cell 0)
          :below (+ (elt ,origin-cell 0) (1+ (elt ,camera-tile-resolution 0))) :do
            (loop :for ,y-sym :from (elt ,origin-cell 1)
               :below (+ (elt ,origin-cell 1) (1+ (elt ,camera-tile-resolution 0))) :do
                 (let ((,cell-pos-sym (vector ,x-sym ,y-sym)))
                   ,@body))))))
