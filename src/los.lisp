(in-package #:dmomd)

;;;; https://legends2k.github.io/2d-fov/design.html

;;;; Cell
;;;;-----------------------------------------------------------------------------
(defclass cell ()
  ((blocker-p
    :accessor cell-blocker-p
    :initarg :blocker-p
    :initform nil)
   (edge-exists
    :accessor cell-edge-exists
    :initarg :edge-exists
    :initform (make-array 4))
   (edge-ids
    :accessor cell-edge-ids
    :initarg :edge-ids
    :initform (make-array 4)))
  (:documentation "This cell datastructure is used to create edges along walls"))

(defmethod print-object ((c cell) out)
  (with-slots (blocker-p
               edge-exists
               edge-ids)
      c
    (print-unreadable-object (c out)
      (format out "blocker-p: ~A, edge-exists: ~A, edge-ids: ~A"
              blocker-p
              edge-exists
              edge-ids))))

;;;; Edge
;;;;-----------------------------------------------------------------------------
(defclass edge ()
  ((sx :initarg :sx :accessor sx)
   (sy :initarg :sy :accessor sy)
   (ex :initarg :ex :accessor ex)
   (ey :initarg :ey :accessor ey)))

(defmethod print-object ((c edge) out)
  (with-slots (sx sy ex ey)
      c
    (print-unreadable-object (c out)
      (format out "sx: ~A, sy: ~A, ex: ~A ey: ~A"
              sx sy ex ey))))

(defun make-edge (sx sy ex ey)
  (make-instance 'edge :sx sx :sy sy :ex ex :ey ey))

(defmethod edge-length ((e edge))
  (with-slots (sx sy ex ey)
      e
    (sqrt (+ (square (abs (- sx ex)))
             (square (abs (- sy ey)))))))

(defmethod line-intersect-p ((e1 edge) (e2 edge))
  "Checks if `e1' and `e2' intersect.
It's assumed both are infinite lines."
  (with-accessors ((p0x sx)
                   (p0y sy)
                   (p1x ex)
                   (p1y ey))
      e1
    (with-accessors ((p2x sx)
                     (p2y sy)
                     (p3x ex)
                     (p3y ey))
        e2
      (let* ((a1 (- p1y p0y))
             (b1 (- p0x p1x))
             (c1 (+ (* a1 p0x) (* b1 p0y)))
             (a2 (- p3y p2y))
             (b2 (- p2x p3x))
             (c2 (+ (* a1 p2x) (* b2 p2y)))
             (denom (- (* a1 b2) (* a2 b1))))

        (if (zerop denom)
            nil  ; lines are parallel
            (let ((result-x (/ (- (* b2 c1) (* b1 c2)) denom))
                  (result-y (/ (- (* a1 c2) (* a2 c1)) denom)))
              (vector result-x result-y)))))))

;;;; LOS
;;;;-----------------------------------------------------------------------------

(defun make-2d-cell-grid (w h)
  (let ((result (make-array (list w h) :element-type 'cell)))
    (2d-loop-over (result x y)
      (setf (aref result x y)
            (make-instance 'cell
                           :edge-exists (make-array 4 :initial-element nil)
                           :edge-ids    (make-array 4 :initial-element nil))))
    result))

(defvar *curr-edge-id* 0)
(defvar *edge-colors* (make-hash-table))
(defvar *angle-points* nil)
(defparameter *cell-grid* (make-2d-cell-grid *dungeon-width* *dungeon-height*))

;;; The LOS Function
(defun calc-los-visible-area ()
  "returns a list of all cell-positions that are inside vision area."
  (setf *visible-cells* nil)
  (let* (result
         (pp (player-pos))
         (ppx (elt pp 0))
         (ppy (elt pp 1))
         (vcl (visible-cells-to-the-left  ppx ppy))
         (vcr (visible-cells-to-the-right ppx ppy))
         (vcu (visible-cells-to-the-up    ppx ppy))
         (vcd (visible-cells-to-the-down  ppx ppy))
         (all-except-cross (calc-diagonal-cell-positions pp (vision-radius *player*)))
         (result (nconc result
                        vcl
                        vcr
                        vcu
                        vcd
                        all-except-cross)))
        (nintersection result
                   (relatives-to-absolutes
                    (player-pos)
                    (mask-to-relative
                     (vision-mask *player*)))
                   :test #'equalp)))

(defun populate-cell-grid (cell-grid grid)
  "takes a dungen:stage and returns an array of polygon edges."
  (2d-loop-over (grid x y)  ; loop over the stage
    (if (is-blocker-p (aref grid x y)) ; only a door or a wall can have blocking edges
        (progn
          (setf (cell-blocker-p (aref cell-grid x y)) t)
          (when (or (not (blocker-north-from-p (vector x y)))
                    (door-north-from-p (vector x y)))
            (populate-edge-north x y))
          (when (or (not (blocker-south-from-p (vector x y)))
                    (door-south-from-p (vector x y)))
            (populate-edge-south x y))
          (when (or (not (blocker-east-from-p (vector x y)))
                    (door-east-from-p (vector x y)))
            (populate-edge-east  x y))
          (when (or (not (blocker-west-from-p (vector x y)))
                    (door-west-from-p (vector x y)))
            (populate-edge-west  x y))))))

(defun populate-edge-pool ()
  "used *cell-grid* to populate *edge-pool*"
  (2d-loop-over (*cell-grid* x y)
    (let ((curr-pos  (vector x y))
          (north-pos (vector  x (1+ y)))
          (south-pos (vector  x (1- y)))
          (east-pos  (vector (1+ x) y))
          (west-pos  (vector (1- x) y)))
      (when (cell-blocker-p (aref *cell-grid* x y))  ; check if curr cell is a blocker
        (when (north-edge-exists curr-pos)
          (if (and (has-neighbor-p :west curr-pos)
                   (north-edge-exists west-pos))
              (extend-north-edge-from-west x y)
              (create-new-edge :north curr-pos)))
        (when (south-edge-exists curr-pos)
          (if (and (has-neighbor-p :west  curr-pos)
                   (south-edge-exists west-pos))
              (extend-south-edge-from-west x y)
              (create-new-edge :south curr-pos)))
        (when (east-edge-exists  curr-pos)
          (if (and (has-neighbor-p :south curr-pos)
                   (east-edge-exists south-pos))
              (extend-east-edge-from-south x y)
              (create-new-edge :east curr-pos)))
        (when (west-edge-exists  curr-pos)
          (if (and (has-neighbor-p :south curr-pos)
                   (west-edge-exists south-pos))
              (extend-west-edge-from-south x y)
              (create-new-edge :west curr-pos)))))))

(defun populate-poly-edges (grid &key (edge-pool *edge-pool*))
  "creates a polymap"
  ;; grid ---> dungeon-stage-grid
  ;; cell-grid ---> grid of cells used in the creation of *edge-pool*
  (reset-grid *cell-grid*)
  (populate-cell-grid *cell-grid* grid)
  (create-edge-colors *curr-edge-id*)
  (populate-edge-pool)
  (setf *angle-points* (create-angle-points *edge-pool*)))
