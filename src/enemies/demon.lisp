(in-package #:dmomd)

(defvar *animation-battle-demon-idle* nil)
(defvar *animation-battle-demon-hurt* nil)
(defvar *animation-battle-demon-dying* nil)
(defvar *animation-battle-demon-attack-demon-slash* nil)


(defclass demon (enemy) ()
  (:default-initargs
   :hp       100
    :hp-max  100

    :xp 0
    :xp-total 666


    :agility       6
    :strength      11
    :intelligence  4
    :initiative    5
    :dungeon-pos nil

    :battle-draw-pos (tgk:vec2 800 200)
    :physical-defense  3
    :vulnerabilities  (list :water)
    :resistances      (list :fire :lighting)))

(defmethod initialize-instance :after ((d demon) &key)
  (setf (vision-mask d) (make-mask (vision-radius d)))
  ;; (setf (current-animation d)
  ;;       *animation-dungeon-demon-idle*)
  (setf (current-animation d)
        *animation-battle-demon-idle*)
  (setf (dungeon-animations d) nil)
  (setf (battle-animations d)
        (list (cons :idle
                    *animation-battle-demon-idle*)
              (cons :hurt
                    *animation-battle-demon-hurt*)
              (cons :dying
                    *animation-battle-demon-dying*)
              (cons :attack-demon-slash
                    *animation-battle-demon-attack-demon-slash*)))
  ;; (setf (battle-attack-effect-animations d)
  ;;       (list (cons :attack-slash
  ;;                   *animation-battle-demon-attack-slash-effect*)))
  (setf (attacks d)
        (list (cons :attack-demon-slash *attack-demon-slash*)))
  (setf (hp d) (hp-max d))
  (setf (mp d) (mp-max d)))

(defmacro make-demon (&rest args)
  `(make-instance 'demon ,@args))

(defmethod print-object ((d demon) out)
  (with-slots (hp hp-max dungeon-pos)
      d
    (print-unreadable-object (d out)
      (format out "Demon: ~A/~A at ~A" hp hp-max dungeon-pos))))


(defmethod attack :before ((d demon) (attacked can-fight) atk-keyword)
  (play-sound (attack-sound (get-attack atk-keyword d)))
  (play-sound :snd-demon-attack-1)
  ;; start attack animation
  (setf (current-animation d)
        (get-animation atk-keyword (battle-animations d)))
  (start-animation (current-animation d) (now))
  ;; start attack effect animation
  ;; (push (get-animation atk-keyword (battle-attack-effect-animations d)) *effect-animations*)
  ;; (start-animation (get-animation atk-keyword (battle-attack-effect-animations d)) (now))
  )

(defmethod attack ((d demon) (e can-fight) atk-keyword)
  (call-next-method d e atk-keyword))

(defmethod damage-for (amount (attacked demon))
  (if (eq :infinity amount)
      (setf (hp attacked) 0)
      (setf (hp attacked) (- (hp attacked) amount)))
  (if (zerop (hp attacked))
      (play-sound :snd-demon-death)
      (play-sound :snd-demon-hurt-1)))

(defmethod draw ((e demon) &optional (draw-pos (battle-draw-pos e)))
  (render-animation  (current-animation e)
                     e
                     draw-pos))

(defmethod render-animation ((a animation) (d demon) window-pos)
  (unless (null a)
    (let* ((pos (position-of a))
           (frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame)))
      (tgk:with-pushed-canvas ()
        (tgk:translate-canvas (tgk:x pos) (tgk:y pos))
        ;; draw appropriate frame/sprite
        (draw-sprite (keyframe-image frame)
                     (tgk:add window-pos origin)))
      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (setf (attacking-p d) nil)
        (if (dead-p d)
            (setf (current-animation d)
                  *animation-battle-demon-dying*)
            (setf (current-animation d)
                  *animation-battle-demon-idle*))))))

;;;;-----------------------------------------------------------------------------
;;;; Define Animations
;;;;-----------------------------------------------------------------------------
(defun create-animations-demon ()
  "You need to make sure none of the animations is looping, except for the idle animation."

  (setf *animation-battle-demon-idle*
        (make-animation "battle-demon-idle"
                        (list
                         (list (get-resource '(:img :battle :demon :idle)) 0
                               (tgk:vec2 -170 -100) nil nil))
                        .5 :looped-p t))

  (setf *animation-battle-demon-hurt*
        (make-animation "battle-demon-hurt"
                        (list
                         (list (get-resource '(:img :battle :demon :hurt)) 0
                               (tgk:vec2 -200 -50) nil nil))
                        .4 :looped-p nil))

  (setf *animation-battle-demon-dying*
        (make-animation
         "battle-demon-dying"
                        (list
                         (list (get-resource '(:img :battle :demon :dying-0)) 0
                               (tgk:vec2 0 0) nil nil)
                         (list (get-resource '(:img :battle :demon :dying-1)) .5
                               (tgk:vec2 -205 0) nil nil)
                         (list (get-resource '(:img :battle :demon :dying-2)) .9
                               (tgk:vec2 -361 5) nil nil)
                         (list (get-resource '(:img :battle :demon :dying-3)) 1.7
                               (tgk:vec2 -450 -27) nil nil)
                         (list (get-resource '(:img :battle :demon :dying-4)) 2.1
                               (tgk:vec2 -522 -12) nil nil))
                        5 :looped-p nil
                        :position (tgk:vec2 -300 -100)))

  (setf *animation-battle-demon-attack-demon-slash*
        (make-animation "battle-demon-attack-demon-slash"
                        (list
                         (list (get-resource '(:img :battle :demon :attack-slash)) 0
                               (tgk:vec2 -650 -60) nil nil))
                        .5 :looped-p nil)))
