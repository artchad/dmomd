(in-package #:dmomd)


(defclass enemy (animatable positionable can-fight)
  ()
  (:default-initargs
   :battle-draw-pos (gamekit:vec2 800 200)))

(defmethod print-object ((e enemy) out)
  (with-slots (hp hp-max dungeon-pos)
      e
    (print-unreadable-object (e out)
      (format out "Enemy: ~A/~A at ~A" hp hp-max dungeon-pos))))

(defmethod move-to ((e enemy) to)
  "change enemy-position")

(defun enemy-vision-radius (enemy-type)
  (case enemy-type
    (slime 2.5)
    (rat   3.4)
    (demon 5)))

(defun spawn-enemies ()
  (spawn-slime *boss-room-region* :num 1)
  (spawn-rats))

(defun draw-enemy-health-bars (enemies)
  (dolist (e enemies)
    (draw-enemy-health-bar e)))

(defun draw-enemy-health-bar (e &key (curr-hp (hp e))
                                  (max-hp (hp-max e))
                                  (draw-pos *window-center*))
  (let* ((width 150)
         (height 10)
         (hp-ratio (/ curr-hp max-hp))
         (max-hp-scale-factor (/ width max-hp))
         (hp-scale-factor (if (zerop curr-hp) 0 (/ width curr-hp))))
    (if *in-boss-battle-p*
        (let* ((width 400)
              (height 25)
              (draw-pos (tgk:vec2 800 100))
               (max-hp-scale-factor (/ width max-hp))
              (hp-scale-factor (if (zerop curr-hp) 0 (/ width curr-hp))))
          (gamekit:draw-rect draw-pos
                             (* max-hp  max-hp-scale-factor)
                             height
                             :fill-paint (gamekit:vec4 .5 0 0 1)
                             :stroke-paint(gamekit:vec4 .3 0 0 1)
                             :thickness 8)
          (gamekit:draw-rect draw-pos
                             (floor (* (* curr-hp hp-scale-factor) hp-ratio))
                             height
                             :fill-paint (gamekit:vec4 .75 0 0 1))
          (draw-image (tgk:subt draw-pos
                                (tgk:vec2 77 37))
                      :img-ui-demon-hp-bar-overlay))
        (let ((hp-bar-pos (tgk:add draw-pos (tgk:vec2 0 250))))
          (gamekit:draw-rect hp-bar-pos
                             (* max-hp  max-hp-scale-factor)
                             height
                             :fill-paint (gamekit:vec4 .5 0 0 1)
                             :stroke-paint(gamekit:vec4 .2 .05 .05 1)
                             :thickness 5)
          (gamekit:draw-rect hp-bar-pos
                             (floor (* (* curr-hp hp-scale-factor) hp-ratio))
                             height
                             :fill-paint (gamekit:vec4 .75 0 0 1))
          (draw-image (tgk:subt hp-bar-pos
                                (tgk:vec2 6 5))
                      :img-ui-enemy-hp-bar-overlay)))))

(defun all-enemies-dead-p (enemies)
  (if (null enemies)
      (return-from all-enemies-dead-p t)
      (dolist (e enemies t)
        (unless (dead-p e)
          (return-from all-enemies-dead-p nil)))))

(defun enemy-choose-attack-keyword (enemy)
  (car (a:random-elt (attacks enemy))))

(defun spawn-slime (region-id &key (num 1))
  (cond ((not (positive-p num))
         (error "num should be a positive integer, instead I got ~a." num))
        ;;----------------------------------------------------------
        ((= region-id *boss-room-region*)
         (let ((spawn-positions (take num
                                      (a:shuffle (adjacent-spawn-positions
                                                  (dungeon-pos *treasure*))))))
           (unless spawn-positions
             (error "There aren't ~A valid spawn positions." num))
           (dotimes (i num)
             (push (make-instance 'slime
                                  :dungeon-pos (elt spawn-positions i)
                                  :room-id region-id)
                   *enemies*))))
        ;;----------------------------------------------------------
        (t (let ((spawn-positions (a:shuffle (take num (gethash region-id *dungeon-rooms*)))))
             (dolist (spawn-pos spawn-positions)
               (push (make-instance 'slime
                                    :dungeon-pos spawn-pos
                                    :room-id (region-of spawn-pos))
                     *enemies*))))))

(defun spawn-rats (&optional (region-ids *dungeon-empty-rooms*))
  (dolist (r region-ids)
    (unless (= r *boss-room-region*)
      (with-percent-chance-of (80)
        (let ((random-position (a:random-elt (gethash r *dungeon-rooms*))))
          (push (make-instance 'rat
                               :dungeon-pos random-position
                               :room-id (region-of random-position))
                *enemies*))))))
