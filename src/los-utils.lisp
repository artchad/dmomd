(in-package #:dmomd)


(defmacro get-edge-exists (direction x y &optional (grid *cell-grid*))
  "used to access edge-ids"
  `(aref (cell-edge-exists (aref ,grid ,x ,y)) ,direction))

(defun north-edge-exists (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-exists (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *north*))

(defun south-edge-exists (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-exists (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *south*))

(defun east-edge-exists (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-exists (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *east*))

(defun west-edge-exists (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-exists (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *west*))


;;;; Get edge-id
;;;;-----------------------------------------------------------------------------
(defmacro get-edge-id (direction x y &optional (grid *cell-grid*))
  "used to access edge-ids"
  `(aref (cell-edge-ids (aref ,grid ,x ,y)) ,direction))

(defun north-edge-id (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-ids (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *north*))

(defun south-edge-id (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-ids (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *south*))

(defun east-edge-id (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-ids (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *east*))

(defun west-edge-id (curr-pos &optional (cell-grid *cell-grid*))
  (aref (cell-edge-ids (aref cell-grid (elt curr-pos 0) (elt curr-pos 1)))
        *west*))

(defun extend-edge-from (from cell-pos edge-direction)
  "`from' can be :north, :south, :east, :west.
`cell-pos' is a 2D-vector
`edge-direction' can be :north, :south, :east, :west."
  (let ((x (elt cell-pos 0))
        (y (elt cell-pos 0)))
   (case edge-direction
     (:north (extend-north-edge-from-west x y))
     (:south (extend-south-edge-from-west x y))
     (:east  (extend-east-edge-from-south x y))
     (:west  (extend-west-edge-from-south x y)))))

(defun extend-north-edge-from-west (x y)
  "Gets the cell to the left and uses it's north-edge-value
to retreive the edge from *edge-pool* and then modifies the
retreive edge."
  (symbol-macrolet ((curr-cell (aref *cell-grid* x y))
                    (west-cell (aref *cell-grid* (1- x) y)))
    (with-slots (sx sy ex ey)
        (gethash (aref (cell-edge-ids west-cell) *north*) *edge-pool*)
      (incf ex))))

(defun extend-south-edge-from-west (x y)
  (symbol-macrolet ((curr-cell (aref *cell-grid* x y))
                    (west-cell (aref *cell-grid* (1- x) y)))
    (with-slots (sx sy ex ey)
        (gethash (aref (cell-edge-ids west-cell) *south*) *edge-pool*)

      (incf ex))))

(defun extend-east-edge-from-south (x y)
  (symbol-macrolet ((curr-cell (aref *cell-grid* x y))
                    (south-cell (aref *cell-grid* x (1- y))))
    (with-slots (sx sy ex ey)
        (gethash (aref (cell-edge-ids south-cell) *east*) *edge-pool*)

      (incf ey))))

(defun extend-west-edge-from-south (x y)
  (symbol-macrolet ((curr-cell (aref *cell-grid* x y))
                    (south-cell (aref *cell-grid* x (1- y))))
    (with-slots (sx sy ex ey)
        (gethash (aref (cell-edge-ids south-cell) *west*) *edge-pool*)

      (incf ey))))

(defun create-new-edge (direction curr-pos)
  (let ((x (elt curr-pos 0))
        (y (elt curr-pos 1)))
    (case direction
     (:north (setf (gethash (aref (cell-edge-ids (aref *cell-grid* x y)) *north*) *edge-pool*)
                   (make-instance 'edge
                                  :sx x      :sy (1+ y)
                                  :ex (1+ x) :ey (1+ y))))
     (:south (setf (gethash (aref (cell-edge-ids (aref *cell-grid* x y)) *south*) *edge-pool*)
                   (make-instance 'edge
                                     :sx x      :sy y
                                     :ex (1+ x) :ey y)))
     (:east  (setf (gethash (aref (cell-edge-ids (aref *cell-grid* x y)) *east*) *edge-pool*)
              (make-instance 'edge
                              :sx (1+ x) :sy y
                              :ex (1+ x) :ey (1+ y))))
     (:west  (setf (gethash (aref (cell-edge-ids (aref *cell-grid* x y)) *west*) *edge-pool*)
              (make-instance 'edge
                              :sx x      :sy y
                              :ex x      :ey (1+ y)))))))

(defmacro set-edge-exists (value direction curr-pos &optional (cell-grid *cell-grid*))
  `(setf (aref (cell-edge-exists (aref ,cell-grid (elt ,curr-pos 0) (elt ,curr-pos 1)))
               ,direction)
         ,value))

(defun populate-edge (direction-keyword x y)
  (unless (cell-to-the direction-keyword (vector x y)) ; check if the cell exists
    ;; we need to add an edge
    (case direction-keyword
      (:north (populate-edge-north x y))
      (:south (populate-edge-south x y))
      (:east  (populate-edge-east  x y))
      (:west  (populate-edge-west  x y)))))

(defun populate-edge-north (x y)
  "The current cell should have a north edge.
Either create it or use another cells edge-id."
  (let ((curr-pos (vector x y))
        (west-pos (vector (1- x) y)))
    (setf (get-edge-exists *north* x y *cell-grid*) t)
    (if (and (blocker-west-from-p  curr-pos)
             (north-edge-exists west-pos))
        ;; extend edge from west if possible
        (setf (get-edge-id *north* x y *cell-grid*)
              (get-edge-id *north* (1- x) y *cell-grid*))
        ;; just create an edge
        (progn
          (setf (get-edge-id *north* x y *cell-grid*) *curr-edge-id*)
          (incf *curr-edge-id*)))))


(defun populate-edge-south (x y)
  (let ((curr-pos (vector x y))
        (west-pos (vector (1- x) y)))
    (setf (get-edge-exists *south* x y *cell-grid*) t)
    (unless (cell-outside-grid-p west-pos)
      (if (and (blocker-west-from-p  curr-pos)
               (south-edge-exists west-pos))
          (setf (get-edge-id *south* x y *cell-grid*)
                (get-edge-id *south* (1- x) y *cell-grid*))
          ;; else create a new edge
          (progn
            (setf (get-edge-id *south* x y *cell-grid*) *curr-edge-id*)
            (incf *curr-edge-id*))))))


(defun populate-edge-east (x y)
  (let ((curr-pos (vector x y))
        (south-pos (vector x (1- y))))
    (setf (get-edge-exists *east* x y *cell-grid*) t)
    (unless (cell-outside-grid-p south-pos)
      (if (and (blocker-south-from-p  curr-pos)
               (east-edge-exists south-pos))
          (setf (get-edge-id *east* x y *cell-grid*)
                (get-edge-id *east* x (1- y) *cell-grid*))
          ;; else create a new edge
          (progn
            (setf (get-edge-id *east* x y *cell-grid*) *curr-edge-id*)
            (incf *curr-edge-id*))))))


(defun populate-edge-west (x y)
  (let ((curr-pos (vector x y))
        (south-pos (vector x (1- y))))
    (setf (get-edge-exists *west* x y *cell-grid*) t)
    (unless (cell-outside-grid-p south-pos)
      (if (and (blocker-south-from-p  curr-pos)
               (west-edge-exists south-pos))
          (setf (get-edge-id *west* x y *cell-grid*)
                (get-edge-id *west* x (1- y) *cell-grid*))
          ;; else create a new edge
          (progn
            (setf (get-edge-id *west* x y *cell-grid*) *curr-edge-id*)
            (incf *curr-edge-id*))))))


(defun reset-grid (&optional (cell-grid *cell-grid*))
  (setf *curr-edge-id* 0)
  (setf *edge-pool* (make-hash-table))
  (2d-loop-over (cell-grid x y)
    (setf (cell-blocker-p (aref cell-grid x y)) nil)
    (do ((j 0 (1+ j)))
        ((>= j 4))
      ;; reset all cells in the region of interest
      (setf (aref (cell-edge-exists (aref cell-grid x y)) j) nil)  ; set edge-exists to nil
      (setf (aref (cell-edge-ids    (aref cell-grid x y)) j) nil)  ; set edge-id to nil
      )))



(defun create-angle-points (edge-pool)
  "The created angle points are not coordinates on the grid,
but points on a grid."
  (let (result)
    (do-hash-table (k v edge-pool)
      (with-slots (sx sy ex ey) v
        (pushnew (vector sx sy) result)
        (pushnew (vector ex ey) result)))
    result))


(defun potentially-blocking-p (edge x y)
  "checks if the cell at (x/y) can be blocked
 by `edge' from the view of the player."
  (with-slots (sx sy ex ey)
      edge
    (let* ((player-pos (dungeon-pos *player*))
           (px (elt player-pos 0))
           (py (elt player-pos 1)))
      ;; check for x
      (if (and (>= px x)
               (or (> sx x)
                   (> ex x)))
          (return-from potentially-blocking-p t))

      (if (and (< px x)
               (or (<= sx x)
                   (<= ex x)))
          (return-from potentially-blocking-p t))

      ;; check for y
      (if (and (>= py y)
               (or (> sy y)
                   (> ey y)))
          (return-from potentially-blocking-p t))

      (if (and (< py y)
               (or (<= sy y)
                   (<= ey y)))
          (return-from potentially-blocking-p t)))))


(defun cell-in-los-p (cell-pos)
  "doc"
  (when (equalp cell-pos (dungeon-pos *player*))
    (return-from cell-in-los-p t))
  (let* ((player-pos (dungeon-pos *player*))
         (player-pos-x (elt player-pos 0))
         (player-pos-y (elt player-pos 1))
         (player-x (+ 0.5 (elt player-pos 0)))
         (player-y (+ 0.5 (elt player-pos 1)))
         (cell-pos-x (elt cell-pos 0))
         (cell-pos-y (elt cell-pos 1))
         (cell-x (+ .5 cell-pos-x))
         (cell-y (+ .5 cell-pos-y))
         ;; (nearest-edges (calc-nearest-edges player-pos))
         )
    t
    ;; (if (= (cell-region-id (aref *grid*
    ;;                              (a:clamp cell-pos-x 0 10)
    ;;                              (a:clamp cell-pos-y 0 10)))
    ;;        (cell-region-id (aref *grid* (elt player-pos 0) (elt player-pos 1))))
    ;;     t
    ;;     nil)
    ;; (dolist (e nearest-edges t)
    ;;   (when (line-intersect-p e (make-edge cell-x cell-y player-x player-y))
    ;;     (return-from cell-in-los-p nil)))
    ))



(defun pos-to-the-right-p (pos-x x)
  "Checks if (pos-x/pos-y) is to the right of (x/y)."
  (if (> pos-x x) t nil))

(defun pos-to-the-left-p (pos-x x)
  "Checks if (pos-x/pos-y) is to the left of (x/y)."
  (if (< pos-x x) t nil))

(defun pos-above-p (pos-y y)
  "Checks if (pos-x/pos-y) is above (x/y)."
  (if (> pos-y y) t nil))

(defun pos-below-p (pos-y y)
  "Checks if (pos-x/pos-y) is below (x/y)."
  (if (< pos-y y) t nil))

(defun pos-on-horizontal-line-with-p (pos-y y)
  (if (= pos-y y) t nil))

(defun pos-on-vertical-line-with-p (pos-x x)
  (if (= pos-x x) t nil))


(defun edge-left-to-p (edge x)
  (with-slots (sx sy ex ey)
      edge
    ;; `<=' because edges lay on the grid
    ;; positions inside of cells
    ;; there's an offset of .5
    (if (and (<= sx x) (<= ex x))
        t
        nil)))

(defun edge-right-to-p (edge x)
  (with-slots (sx sy ex ey)
      edge
    (if (and (> sx x) (> ex x))
        t
        nil)))

(defun edge-above-p (edge y)
  (with-slots (sx sy ex ey)
      edge
    (if (and (> sy y) (> ey y))
        t
        nil)))

(defun edge-below-p (edge y)
  (with-slots (sx sy ex ey)
      edge
    (if (and (<= sy y) (<= ey y))
        t
        nil)))


(defun calc-nearest-edges (pos)
  (let ((x (elt pos 0))
        (y (elt pos 1)))
    (list
     (get-nearest-edge-north x y)
     (get-nearest-edge-south x y)
     (get-nearest-edge-east  x y)
     (get-nearest-edge-west  x y))))


(defun get-nearest-edge-north (x y)
  "doc"
  (dotimes (i *dungeon-height*)
    (let ((curr-pos (vector x (+ y i))))
      (if (south-edge-exists curr-pos)
         (return-from get-nearest-edge-north
           (gethash (south-edge-id curr-pos)
                    *edge-pool*))
         (if (north-edge-exists curr-pos)
             (return-from get-nearest-edge-north
               (gethash (north-edge-id curr-pos)
                        *edge-pool*)))))))


(defun get-nearest-edge-south (x y)
  "doc"
  (dotimes (i *dungeon-height*)
    (let ((curr-pos (vector x (- y i))))
      (if (north-edge-exists curr-pos)
          (return-from get-nearest-edge-south
            (gethash (north-edge-id curr-pos)
                     *edge-pool*))
          (if (south-edge-exists curr-pos)
              (return-from get-nearest-edge-south
                (gethash (south-edge-id curr-pos)
                         *edge-pool*)))))))


(defun get-nearest-edge-east (x y)
  "doc"
  (dotimes (i *dungeon-width*)
    (let ((curr-pos (vector (+ x i) y)))
      (if (west-edge-exists curr-pos)
          (return-from get-nearest-edge-east
            (gethash (west-edge-id curr-pos)
                     *edge-pool*))
          (if (east-edge-exists curr-pos)
              (return-from get-nearest-edge-east
                (gethash (east-edge-id curr-pos)
                         *edge-pool*)))))))


(defun get-nearest-edge-west (x y)
  "doc"
  (dotimes (i *dungeon-width*)
    (let ((curr-pos (vector (- x i) y)))
      (if (east-edge-exists curr-pos)
          (return-from get-nearest-edge-west
            (gethash (east-edge-id curr-pos)
                     *edge-pool*))
          (if (west-edge-exists curr-pos)
              (return-from get-nearest-edge-west
                (gethash (west-edge-id curr-pos)
                         *edge-pool*)))))))


(defun visible-cells-north-west (x y)
  (if (is-blocker-p (aref *grid* x y))
      (list (vector x y))
   (cons (vector x y)
         (union (visible-cells-to-the-up x y)
                (visible-cells-to-the-left x y)))))

(defun visible-cells-north-east (x y)
  (if (is-blocker-p (aref *grid* x y))
      (list (vector x y))
   (cons (vector x y)
         (union (visible-cells-to-the-up x y)
                (visible-cells-to-the-right x y)))))

(defun visible-cells-south-west (x y)
  (if (is-blocker-p (aref *grid* x y))
      (list (vector x y))
      (cons (vector x y)
         (union (visible-cells-to-the-down x y)
                (visible-cells-to-the-left x y)))))

(defun visible-cells-south-east (x y)
  (if (is-blocker-p (aref *grid* x y))
      (list (vector x y))
      (cons (vector x y)
         (union (visible-cells-to-the-down x y)
                (visible-cells-to-the-right x y)))))


(defun visible-cells-to-the-left  (x y &optional (call-others nil))
  (let ((result
         (list (vector x y))))
    (dotimes (i *dungeon-width*)
      (block dotimes
        (when (zerop i)
          (return-from dotimes))
        (if (is-blocker-p (aref *grid* (- x i) y))
            (progn
              (push (vector (- x i) y) result)
              (return-from visible-cells-to-the-left result))
            (progn
              (push (vector (- x i) y) result)
              (when call-others
                (nconc result
                       (visible-cells-to-the-up   (- x i) (1+ y) nil)
                       (visible-cells-to-the-down (- x i) (1- y) nil)))))))))


(defun visible-cells-to-the-right (x y &optional (call-others nil))
  (let ((result (list (vector x y))))
    (dotimes (i *dungeon-width*)
      (block dotimes
        (when (zerop i)
          (return-from dotimes))
        (if (is-blocker-p (aref *grid* (+ x i) y))
            (progn
              (push (vector (+ x i) y) result)
              (return-from visible-cells-to-the-right result))
            (progn
              (push (vector (+ x i) y) result)
              (when call-others
                (nconc result
                       (visible-cells-to-the-up (+ x i) y nil)
                       (visible-cells-to-the-down (+ x i) y nil)))))))))


(defun visible-cells-to-the-up    (x y &optional (call-others nil))
  (let ((result (list (vector x y))))
    (dotimes (i *dungeon-height*)
      (block dotimes
        (when (zerop i)
          (return-from dotimes))
        (if (is-blocker-p (aref *grid* x (+ y i)))
            (progn
              (push (vector x (+ y i)) result)
              (return-from visible-cells-to-the-up result))
            (progn
              (push (vector x (+ y i)) result)
              (when call-others
                (nconc result
                       (visible-cells-to-the-left  x (+ y i) nil)
                       (visible-cells-to-the-right x (+ y i) nil)))))))))


(defun visible-cells-to-the-down  (x y &optional (call-others nil))
  (let ((result (list (vector x y))))
    (dotimes (i *dungeon-height*)
      (block dotimes
        (when (zerop i)
          (return-from dotimes))
        (if (is-blocker-p (aref *grid* x (- y i)))
            (progn
              (push (vector x (- y i)) result)
              (return-from visible-cells-to-the-down result))
            (progn
              (push (vector x (- y i)) result)
              (when call-others
                (nconc result
                       (visible-cells-to-the-left  x (- y i) nil)
                       (visible-cells-to-the-right x (- y i) nil)))))))))

(defun calc-diagonal-cell-positions (center-pos radius)
  (let* (result
         (x (elt center-pos 0))
         (y (elt center-pos 1))
         (dne (calc-diagonal :north-east radius x y))
         (dnw (calc-diagonal :north-west radius x y))
         (dse (calc-diagonal :south-east radius x y))
         (dsw (calc-diagonal :south-west radius x y)))

    (dolist (e dne)
      (setf result (append result (visible-cells-north-east (elt e 0) (elt e 1)))))
    (dolist (e dnw)
      (setf result (append result (visible-cells-north-west (elt e 0) (elt e 1)))))
    (dolist (e dse)
      (setf result (append result (visible-cells-south-east (elt e 0) (elt e 1)))))
    (dolist (e dsw)
      (setf result (append result (visible-cells-south-west (elt e 0) (elt e 1)))))
    result))


(defun calc-diagonal (keyword radius x y)
  (let (result
        (diag-num (round (/ radius +sqrt-2+))))

    (case keyword
      (:north-east
       (dotimes (i diag-num)
         (let ((new-x (+ x 1 i))
               (new-y (+ y 1 i)))
           (if (is-blocker-p (aref *grid* new-x new-y))
               (return-from calc-diagonal (push (vector new-x new-y) result))
               (push (vector new-x new-y) result)))))

      (:north-west
       (dotimes (i diag-num)
         (let ((new-x (- x 1 i))
               (new-y (+ y 1 i)))
           (if (is-blocker-p (aref *grid* new-x new-y))
               (return-from calc-diagonal (push (vector new-x new-y) result))
            (push (vector new-x new-y) result)))))

      (:south-east
       (dotimes (i diag-num)
         (let ((new-x (+ x 1 i))
               (new-y (- y 1 i)))
           (if (is-blocker-p (aref *grid* new-x new-y))
               (return-from calc-diagonal (push (vector new-x new-y) result))
               (push (vector new-x new-y) result)))))

      (:south-west
       (dotimes (i diag-num)
         (let ((new-x (- x 1 i))
               (new-y (- y 1 i)))
           (if (is-blocker-p (aref *grid* new-x new-y))
               (return-from calc-diagonal (push (vector new-x new-y) result))
               (push (vector new-x new-y) result))))))
    result))
