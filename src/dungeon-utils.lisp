(in-package #:dmomd)


(defun door-at-p (pos)
  (cell-has-door-p (grid-seq-ref pos)))

(defun wall-at-p (pos)
  (cell-wall-p (grid-seq-ref pos)))

(declaim (inline cell-wall-p))
(defun cell-wall-p (c)
  (member :wall (dungen::cell-features c)))

(declaim (inline cell-hellfire-door-p))
(defun cell-hellfire-door-p (c)
  (member :hellfire-door (dungen::cell-features c)))

(declaim (inline cell-locked-door-p))
(defun cell-locked-door-p (c)
  (member :locked (dungen::cell-features c)))

(declaim (inline cell-features))
(defun cell-features (cell)
  "`cell' is a 2D vector"
  (dungen::cell-features (aref *grid* (elt cell 0)  (elt cell 1))))

(declaim (inline cell-region-id))
(defun cell-region-id (cell)
  (dungen::cell-region cell))

(defun get-dungeon-cell (cell-pos &optional (grid *grid*))
  "`cell' is a 2D vector"
  (aref grid (elt cell-pos 0) (elt cell-pos 1)))

(defun get-dungeon-cell-relative-from-player (cell-pos &optional (player *player*) (grid *grid*))
  "`cell-pos' is a 2D vecor."
  (let* ((player-pos (dungeon-pos player))
         (new-pos (seq+ player-pos cell-pos))
         (x (elt new-pos 0))
         (y (elt new-pos 1)))
    (aref grid x y)))

(defun cell-pos-out-of-bounds-p (pos 2d-array)
  (let* ((dimensions (array-dimensions 2d-array))
         (x-dim (first  dimensions))
         (y-dim (second dimensions))
         (pos-x (elt pos 0))
         (pos-y (elt pos 1)))

    (if (or (or (< pos-x 0) (>= pos-x x-dim))
            (or (< pos-y 0) (>= pos-y y-dim)))
        t
        nil)))

(defun is-blocker-p (cell)
  "This function refers to the global variable *dungeon*."
  (if (null cell)
      'outside-of-grid  ; consider blocker outisde of map boundaries
      (dolist (feature (list :wall :door/horizontal :door/vertical :door))
        (when (dungen::has-feature-p cell feature)
          (return-from is-blocker-p t)))))

(declaim (inline same-region-p))
(defun same-region-p (tile-1 tile-2)
  "Both tiles are a 2D-vector which contain tile coordinates."
  (cond ((or (null tile-1) (null tile-2)) nil)
        ((eql (cell-region-id tile-1)
              (cell-region-id tile-2))
         T)
        (T nil)))

(declaim (inline different-region-p))
(defun different-region-p (tile-1 tile-2)
  "Both tiles are a 2D-vector which contain tile coordinates."
  (not (same-region-p tile-1 tile-2)))

(defun cell-has-door-p (cell)
  (cond ((null cell) nil)
        ((dungen:has-feature-p cell :door/horizontal) T)
        ((dungen:has-feature-p cell :door/vertical) T)
        (T nil)))

(defun cell-has-door/horizontal-p (cell)
  (dungen::has-feature-p cell :door/horizontal))

(defun cell-has-door/vertical-p (cell)
  (dungen::has-feature-p cell :door/vertical))

(defun print-dungeon (&optional (stage *dungeon*))
  (format t "~&")
  (loop :for y :from (1- (dungen::stage-height stage)) :downto 0
     :do (loop :for x :below (dungen::stage-width stage)
            :for cell = (dungen::get-cell stage x y)
            :do (format t "~a"
                        (cond ((dungen::has-feature-p cell :hellfire-door)
                               "X ")
                              ((dungen::has-feature-p cell :door/horizontal)
                               "──")
                              ((dungen::has-feature-p cell :door/vertical)
                               "│ ")
                              ((dungen::has-feature-p cell :stairs/up)
                               "↑↑")
                              ((dungen::has-feature-p cell :stairs/down)
                               "↓↓")
                              ((or (dungen::has-feature-p cell :room)
                                   (dungen::has-feature-p cell :corridor))
                               "  ")
                              ((dungen::has-feature-p cell :wall)
                               "██"))))
       (format t "~%")))

(defun get-random-room (&optional (rooms *dungeon-rooms*))
  "get a random room (list of cells) indexed by
 an integer greater than 0 up to the number of rooms."
  (gethash (1+ (random (hash-table-count rooms))) rooms))

(defmethod random-room ((ht hash-table) &key (player-not-in-room nil))
  (let (random-room-id)
    (if player-not-in-room
        (setf random-room-id (random-room-id
                              *dungeon-rooms*
                              :exclude (room-id-from-cell (dungeon-pos *player*))))
        (setf random-room-id (random-room-id *dungeon-rooms*)))
    (gethash random-room-id ht)))

(defmethod random-room-id ((rooms hash-table) &key (exclude nil))
  "`exclude' is a list of room-IDs"
  (if exclude
      (a:random-elt (remove exclude (num-to-descending-list (hash-table-count rooms))))
      (1+ (random (hash-table-count rooms)))))

(defun cell-outside-grid-p (cell-coordinates)
  "doc"
  (or (or (>= (elt cell-coordinates 0) *dungeon-width*)
          (>= (elt cell-coordinates 1) *dungeon-height*))
      (or (< (elt cell-coordinates 0) 0)
          (< (elt cell-coordinates 1) 0))))

(defun cell-inside-grid-p (cell-coordinates)
  "doc"
  (and (not (or (>= (elt cell-coordinates 0) (array-dimension *grid* 0))
                (>= (elt cell-coordinates 1) (array-dimension *grid* 1))))
       (not (or (< (elt cell-coordinates 0) 0)
                (< (elt cell-coordinates 1) 0)))))

(defun room-id-from-cell (cell)
  (dungen::cell-region (aref *grid* (elt cell 0) (elt cell 1))))

(defun y>0 (cell-pos)
  (> (elt cell-pos 1) 0))

(defun x>0 (cell-pos)
  (> (elt cell-pos 0) 0))

(defun x<n (cell-pos n)
  (< (elt cell-pos 0) n))

(defun y<n (cell-pos n)
  (< (elt cell-pos 1) n))

(defmethod (setf keyword-aref) (edge-arr keyword)
  (case keyword
    (:north (aref edge-arr *north*))
    (:south (aref edge-arr *south*))
    (:east  (aref edge-arr *east*))
    (:west  (aref edge-arr *west*))))

(defun blocker-north-from-p (cell-pos)
  "doc"
  (is-blocker-p
   (grid-seq-ref (seq+ cell-pos #(0 1)))))

(defun blocker-south-from-p (cell-pos)
  "doc"
  (is-blocker-p
   (grid-seq-ref (seq+ cell-pos #(0 -1)))))

(defun blocker-east-from-p (cell-pos)
  (is-blocker-p
   (grid-seq-ref (seq+ cell-pos #(1 0)))))

(defun blocker-west-from-p (cell-pos)
  "doc"
  (is-blocker-p
   (grid-seq-ref (seq+ cell-pos #(-1 0)))))

(defun door-north-from-p (cell-pos)
  (let* ((x (elt cell-pos 0))
         (y (elt cell-pos 1)))
    (when (< y (1- *dungeon-height*))
      (let ((cell-north (aref *grid* x (1+ y))))
        (or (dungen:has-feature-p cell-north :door/horizontal)
            (dungen:has-feature-p cell-north :door/vertical))))))

(defun door-south-from-p (cell-pos)
  (let* ((x (elt cell-pos 0))
         (y (elt cell-pos 1)))
    (when (> y 0)
      (let ((cell-south (aref *grid* x (1- y))))
        (or (dungen:has-feature-p cell-south :door/horizontal)
            (dungen:has-feature-p cell-south :door/vertical))))))

(defun door-east-from-p (cell-pos)
  (let* ((x (elt cell-pos 0))
         (y (elt cell-pos 1)))
    (when (< x (1- *dungeon-width*))
      (let ((cell-east (aref *grid* (1+ x) y)))
        (or (dungen:has-feature-p cell-east :door/horizontal)
            (dungen:has-feature-p cell-east :door/vertical))))))

(defun door-west-from-p (cell-pos)
  (let* ((x (elt cell-pos 0))
         (y (elt cell-pos 1)))
    (when (> x 0)
      (let ((cell-west (aref *grid* (1- x) y)))
        (or (dungen:has-feature-p cell-west :door/horizontal)
            (dungen:has-feature-p cell-west :door/vertical))))))

(defun has-neighbor-p (direction cell-pos)
  (case direction
    (:north (if (blocker-north-from-p cell-pos) t))
    (:south (if (blocker-south-from-p cell-pos) t))
    (:east  (if (blocker-east-from-p cell-pos) t))
    (:west  (if (blocker-west-from-p cell-pos) t))))

(defun grid-seq-ref (seq &optional (grid *grid*))
  "grid sequence ref"
  (if (cell-outside-grid-p seq)
      nil
      (aref grid (elt seq 0) (elt seq 1))))

(defun cell-to-the (direction-keyword cell-pos &optional (cell-grid *cell-grid*))
  "doc"
  (let ((x (elt cell-pos 0))
        (y (elt cell-pos 1))
        (w (array-dimension cell-grid 0))
        (h (array-dimension cell-grid 1)))
    (cond
      ((<  (1- x) 0) nil)
      ((>=  x     w) nil)
      ((<  (1- y) 0) nil)
      ((<=  y     h) nil)
      (t (case direction-keyword
           (:north (aref cell-grid  x (1+ y)))
           (:south (aref cell-grid  x (1- y)))
           (:east  (aref cell-grid (1+ x) y))
           (:west  (aref cell-grid (1- x) y)))))))

(defun cell-visible-p (cell-pos)
  (member cell-pos *currently-visible-cells* :test #'equalp))

(defun cell-seen-p (curr-cell-pos)
  (member curr-cell-pos *seen-cells* :test #'equalp))

(defun cell-carved-p (c)
  (not (cell-wall-p c)))

(defun suitable-position-p (dungeon x y)
  (and (cell-carved-p (aref (dungen:stage-grid dungeon) x y))
       (not (aggressive-cell-p #(x y)))))

(defun find-a-starting-position (dungeon width height)
  "Finds a suitable spawn position."
  ;; BUG: It's possible to spawn into a fight.
  (loop
     (let ((x (random width))
           (y (random height)))
       (if (suitable-position-p dungeon x y)
           (progn (format t "found starting position: ~S~%" (vector x y))
                  (return (vector x y)))
           (format t "not a good starting position: ~S~%" (vector x y))))))

(defun aggressive-cell-p (position)
  "Returns the aggressor or nil"
  (dolist (enemy *enemies*)
    (when (member position (relatives-to-absolutes (enemy-dungeon-position enemy)
                                                   (mask-to-relative *slime-aggression-mask*))
                  :test #'equalp)
      (return-from aggressive-cell-p t)))
  nil)

(defun valid-aggression-tile-pos-p (aggression-tile enemy)
  (when (cell-outside-grid-p aggression-tile)
      (return-from valid-aggression-tile-pos-p nil))

  (and (member aggression-tile *seen-cells* :test #'equalp)
       (or (and (same-cell-region-p aggression-tile (player-pos))
                (same-cell-region-p aggression-tile (dungeon-pos enemy)))
           (and (stands-on-door-p *player*)
                (same-cell-region-p aggression-tile (dungeon-pos enemy))))))

(defun spawn-treasure ()
  (push (setf
         *treasure*
         (make-chest (a:random-elt (gethash *boss-room-region* *dungeon-rooms*))))
        *rogue-mode-items*))

(defun same-cell-region-p (pos1 pos2)
  (eql (dungen::cell-region (aref *grid*
                                  (elt pos1 0)
                                  (elt pos1 1)))
       (dungen::cell-region (aref *grid*
                                  (elt pos2 0)
                                  (elt pos2 1)))))

(defun stands-on-door-p (positionable)
  (cell-has-door-p (grid-seq-ref (dungeon-pos positionable))))

;;;;-----------------------------------------------------------------------------
;;;; Make Dungeon
;;;;-----------------------------------------------------------------------------
(defun make-dungen ()
  (let* ((s (dungen:make-stage :width  *dungeon-width*
                               :height *dungeon-height*
                               :room-extent  7
                               :door-rate    .8))
         (g (dungen:stage-grid s)))
    ;; populate *dungeon-empty-rooms*
    (loop :for x :from 0 :below (dungen:stage-width s) :do
         (loop :for y :from 0 :below (dungen:stage-height s) :do
              (let* ((cell (aref g x y))
                     (region-id (dungen::cell-region cell)))
                ;; All cells that are part of a room are stored under that
                ;; specific room-id in a hash-table called *dungeon-rooms*.
                ;; At the beginning *dungeon-empty-rooms* contains all room IDs.
                (when (dungen::has-feature-p cell :room)
                  (push (vector x y) (gethash region-id *dungeon-rooms*))
                  (pushnew region-id *dungeon-empty-rooms*)))))
    (setf *dungeon* s)
    (setf *grid* (dungen:stage-grid *dungeon*)) ; define *grid* for easier access to the array
    (setf *boss-room-region* (first (last *dungeon-empty-rooms*)))
    (lock-boss-room *boss-room-region*)))


(defun lock-boss-room (region-id)
  (let ((doors (get-room-doors region-id)))
    (lock-doors doors)))

(defun get-room-doors (region-id)
  "returns a list of 2D-position vectors, which are the coordinates of all adjacent doors."
  (let* ((doors nil)
         (room-border nil)
         (room-tiles (gethash region-id *dungeon-rooms*))
         (room-extents (calc-room-extent room-tiles))
         (new-x-min (1- (first  room-extents)))
         (new-x-max (1+ (second room-extents)))
         (new-y-min (1- (third  room-extents)))
         (new-y-max (1+ (fourth room-extents)))) ; `room' is now a list of all positions inside the room
    (loop :for y :from new-y-min :to new-y-max :do
         (if (or (= y new-y-min) (= y new-y-max))
             (loop :for x :from new-x-min :to new-x-max :do
                  (push (vector x y) room-border))
             (loop :for x :from new-x-min :to new-x-max :do
                  (when (or (= x new-x-min) (= x new-x-max))
                    (push (vector x y) room-border)))))
    (dolist (tile room-border doors)
      (if (cell-has-door-p (grid-seq-ref tile *grid*))
          (push tile doors)))))

(defun calc-room-extent (room-tiles)
  (let ((x-min *dungeon-width*)
        (x-max 0)
        (y-min *dungeon-height*)
        (y-max 0))
    (dolist (tile room-tiles)
      (let ((tile-x (elt tile 0))
            (tile-y (elt tile 1)))
        (when (> x-min tile-x) (setf x-min tile-x))
        (when (< x-max tile-x) (setf x-max tile-x))
        (when (> y-min tile-y) (setf y-min tile-y))
        (when (< y-max tile-y) (setf y-max tile-y))))
    (list x-min x-max y-min y-max)))

(defun lock-doors (doors)
  (dolist (door doors)
   (dungen::add-feature (grid-seq-ref door *grid*) :hellfire-door)))

(defun hellfire-door-p (cell)
  (dungen:has-feature-p cell :hellfire-door))

(defun treasure-on-pos-p (pos)
  (let* ((treasure (find-if (lambda (x) (eq (type-of x) 'treasure))
                            *rogue-mode-items*))
         (treasure-pos (and treasure (dungeon-pos treasure))))
    (when treasure-pos
      (equalp pos treasure-pos))))
