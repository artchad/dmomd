(in-package #:dmomd)


;;;;--------------------------------------------------------------------------------
(defclass menu ()
  ((name
    :accessor menu-name
    :initarg :name
    :initform nil
    :documentation "The name must be unique and represented as a keyword.")
   (parent-menu
    :accessor menu-parent-menu
    :initarg :parent-menu
    :initform nil)
   (font
    :accessor menu-font
    :initarg :font
    :initform nil)
   (font-size
    :accessor menu-font-size
    :initarg :font-size
    :initform  40)
   (origin
    :accessor menu-origin
    :initarg :origin
    :initform (tgk:vec2 0 0))
   (width
    :accessor menu-width
    :initarg :width
    :initform 0)
   (height
    :accessor menu-height
    :initarg :height
    :initform 0)
   (background-color
    :accessor menu-background-color
    :initarg :background-color
    :initform (tgk:vec4 0 0 0 0))
   (visibility
    :accessor menu-visibility
    :initarg :visibility
    :initform nil
    :documentation "one of the following keywords: :active :inactive :hidden")
   (drawing-offset
    :accessor drawing-offset
    :initarg :drawing-offset
    :initform (tgk:vec2 0 0)
    :documentation "When drawing the menu items, the offset is added to the start point."))
  (:documentation "A field with text, which is to be drawn on the screen."))

;;;;--------------------------------------------------------------------------------
(defclass info-menu (menu)
  ((text
     :accessor info-menu-text
     :initarg :text
     :initform ""))
  (:documentation "A static menu, which is used to display information."))

(defun info-menu-p (object)
  (eq (type-of object)
      'dmomd::info-menu))

;;;;--------------------------------------------------------------------------------
(defclass menu-item ()
  ((name
    :accessor menu-item-name
    :initarg :name
    :initform (error "Every menu-item needs a unique keyword identifier."))
   (text
    :accessor menu-item-text
    :initarg :text
    :initform "")
   (child-menu
    :accessor menu-item-child-menu
    :initarg :child-menu
    :initform nil)
   (callback
    :accessor menu-item-callback
    :initarg :callback
    :initform (lambda () nil))))

(defmethod print-object ((mi menu-item) out)
  (with-slots (child-menu
               name
               callback)
      mi
    (print-unreadable-object (mi out)
      (format out "~A" name))))

;;;;--------------------------------------------------------------------------------
(defclass list-menu (menu)
  ((parent-menu-item-name
    :accessor menu-parent-menu-item-name
    :initarg :parent-menu-item-name
    :initform nil)
   (items :accessor list-menu-items
          :initarg :items
          :initform (error "A menu needs menu items."))
   (index :accessor list-menu-index
          :initarg :index
          :initform 0)))

(defmethod print-object ((lm list-menu) out)
  (with-slots (name
               parent-menu
               parent-menu-item-name)
      lm
    (print-unreadable-object (lm out)
      (format out "~A" name))))

(defmethod initialize-instance :after ((m list-menu) &key)
  "Make sure the cursor is on the top most menu item."
  (setf (list-menu-index m) 0))

;;;;-----------------------------------------------------------------------------
(defmacro make-list-menu (item-names item-texts item-callbacks &body body)
  "`item-names' is a list of strings.
`item-callbacks' is a list of function with 0 arguments."
  `(let ((items nil))
     ;; create items list
     (loop
        :for item-name :in ,item-names
        :for item-text :in ,item-texts
        :for callback :in ,item-callbacks :do
        ;; create an alist
          (push (make-instance 'menu-item
                               :name item-name
                               :text item-text
                               :callback callback)
                items))
     (make-instance 'list-menu
                    :items (reverse items)
                    ,@body)))

(defmethod menu-parent-menu-item ((m list-menu))
  (let ((item-name (menu-parent-menu-item-name m)))
    (get-menu-item item-name (menu-parent-menu m))))

(defmethod get-menu-item (item-name (m list-menu))
  (labels ((find-item (menu-items)
             (dolist (i menu-items)
               (if (string-equal item-name (menu-item-name i))
                   (return-from find-item i)
                   (find-item (cdr menu-items))))))
    (find-item (list-menu-items m))))

(defmethod next-menu-item :before (m &optional n)
  (play-sound :snd-button-change))

(defmethod next-menu-item (m &optional (n -1))
  "Set the menu index of m to the next item and play button change sound."
  (setf (list-menu-index m)
        (mod (+ n (list-menu-index m))
             (length (list-menu-items m)))))

(defmethod previous-menu-item :before (m &optional n)
  (play-sound :snd-button-change))

(defmethod previous-menu-item (m &optional (n 1))
  "Set the menu index of m to the previous item and play button change sound.
   Calls `next-menu-item' with a change index of 1"
  (next-menu-item m n))

;; (defmethod menu-call-current-menu-item :before (m)
;;   (play-sound :snd-button-click))

(defmethod menu-call-current-menu-item ((m info-menu))
  "Calls the lambda of the current menu item."
  nil)

(defmethod menu-call-current-menu-item ((m list-menu))
  "Calls the lambda of the current menu item."
  (funcall (menu-item-callback (nth (list-menu-index m) (list-menu-items m)))))

(defmethod menu-current-item ((m list-menu))
  (nth (list-menu-index m) (list-menu-items m)))

(defmethod child-menu (n)
  "If the provided menu is nil."
  nil)

(defmethod child-menu ((m list-menu))
  "Return the menu that's the child of the currently selected menu item."
  (menu-item-child-menu (menu-current-item m)))

(defmethod child-menus ((m list-menu))
  "Return the menu that's the child of the currently selected menu item."
  (let (result)
       (dolist (i (list-menu-items m) result)
         (push (menu-item-child-menu i) result))))

(defmethod get-menu-item (keyword (m list-menu))
  (dolist (mi (list-menu-items m))
    (when (eq (menu-item-name mi) keyword)
      (return-from get-menu-item mi))))

;;;;--------------------------------------------------------------------------------
(defmethod draw-menu (m &optional (font nil))
  nil)

(defmethod draw-menu ((m list-menu) &optional (f :quikhand))
  "Goes linearly through each submenu and draws it."
  (let* ((font (if (menu-font m)
                   (menu-font m)
                   f))
         (font-size (menu-font-size m))
         (x-pos (+ (/ (menu-width m) 8) (tgk:x (menu-origin m)) -10))
         (y-pos (+ (tgk:y (menu-origin m)) (menu-height m)
                   (- font-size)))
         (draw-pos (tgk:add (tgk:vec2 x-pos y-pos) (drawing-offset m))))
    (case (menu-visibility m)
      (:hidden nil)
      (:active
       (unless (or (zerop (menu-width m))
                   (zerop (menu-height m)))
         ;; draw-background
         (gamekit:draw-rect (menu-origin m)
                            (menu-width  m)
                            (menu-height m)
                            :fill-paint (tgk:mult 1.2 (menu-background-color m))
                            :stroke-paint (menu-background-color m)
                            :thickness 5
                            :rounding 10))
       (draw-string-list draw-pos
                         font-size
                         (alpha-color .5 +color-navajowhite1+)
                         (font font font-size)
                         font-size
                         (mapcar #'menu-item-text (list-menu-items m))
                         (list-menu-index m)))
      ;; draw with gray color
      (:inactive
       (unless (or (zerop (menu-width m))
                   (zerop (menu-height m)))
         (ge.vg:with-alpha (.75)
           (gamekit:draw-rect (menu-origin m)
                              (menu-width  m)
                              (menu-height m)
                              :fill-paint (tgk:mult 1.2 (menu-background-color m))
                              :stroke-paint (menu-background-color m)
                              :thickness 5
                              :rounding 10)
           (draw-string-list draw-pos
                             font-size
                             (alpha-color .5 +color-navajowhite1+ )
                             (font font font-size)
                             font-size
                             (mapcar #'menu-item-text (list-menu-items m)))))))
    (and (child-menu m) (draw-menu (child-menu m)))))

(defmethod draw-menu ((m info-menu) &optional (f :quikhand))
  "Goes linearly through each submenu and draws it."
  (unless (and (menu-inactive-p (menu-parent-menu m))
               (menu-active-p (menu-parent-menu (menu-parent-menu m))))

    (let* ((font (if (menu-font m)
                     (menu-font m)
                     f))
           (font-size (menu-font-size m))
           (x-pos (+ (/ (menu-width m) 8)
                     (tgk:x (menu-origin m))
                     -15))
           (y-pos (+ (tgk:y (menu-origin m))
                     (- (menu-height m)
                        font-size)))
           (draw-pos (tgk:add (tgk:vec2 x-pos y-pos) (drawing-offset m))))

      ;; draw-background
      (when (and (not (zerop (menu-width m)))
                 (not (zerop (menu-height m))))
        (gamekit:draw-rect (menu-origin m)
                           (menu-width  m)
                           (menu-height m)
                           :fill-paint (tgk:mult 1.2 (menu-background-color m))
                           :stroke-paint (menu-background-color m)
                           :thickness 5
                           :rounding 10))
      (case (menu-visibility m)
        (:active
         (draw-string-list draw-pos
                           font-size
                           +color-dark-goldenrod+
                           (font font (menu-font-size m))
                           (menu-font-size m)
                           (info-menu-text m)))
        (:inactive
         (ge.vg:with-alpha (.5)
           (draw-string-list
               draw-pos
             font-size
             +color-dark-goldenrod+
             (font font (menu-font-size m))
             (menu-font-size m)
             (info-menu-text m)))))
      (and (child-menu m)
           (draw-menu (child-menu m))))))


(let ((previous-menu nil))
 (defun toggle-settings-menu (&optional (new-root-menu previous-menu))
   (print "Toggled Settings Menu")
   (setf (list-menu-index *settings-menu*) 0)
   (if *show-settings-menu* ; we're leaving the settings menu
       (progn (unbind-buttons *mode*)
              (bind-buttons *mode*)
              (setf (root-menu *mode*)
                    (setf (active-menu *mode*) new-root-menu))
              (setf previous-menu nil))
       (progn (unbind-buttons *mode*)
              (bind-settings-buttons *mode*)
              (setf previous-menu (root-menu *mode*))
              (setf (root-menu *mode*)
                    (setf (active-menu *mode*) *settings-menu*))))
   (setf *show-settings-menu*
         (not *show-settings-menu*))))

(defun bind-settings-buttons (mode)
  (setf (bound-buttons mode)
        (list :h :j :k :l :up :down :left :right :enter :escape ))
  (with-bind-buttons ()
    ((:j  :pressed)
     (previous-menu-item (active-menu mode)))
    ((:down  :pressed)
     (previous-menu-item (active-menu mode)))

    ((:k    :pressed)
     (next-menu-item (active-menu mode)))
    ((:up    :pressed)
     (next-menu-item (active-menu mode)))

    ((:h  :pressed)
     (select-menu-previous mode))
    ((:left  :pressed)
     (select-menu-previous mode))

    ((:l :pressed)
     (menu-call-current-menu-item (active-menu mode))
     (select-menu-next mode))
    ((:right :pressed)
     (menu-call-current-menu-item (active-menu mode))
     (select-menu-next mode))
    ((:enter :pressed)
     (menu-call-current-menu-item (active-menu mode))
     (select-menu-next mode))

    ((:escape :pressed) (toggle-settings-menu))))

(defun create-settings-menu ()
  (let ((font-size 40)
        (menu-color (alpha-color .4 (hexcolor "#110501"))))
    (setf *settings-menu*
          (make-list-menu
              '(:return-to-main-menu :quit-game :toggle-music :exit )
              '("Return To Main Menu"
                "Toggle Music"
                "Quit Game"
                "Exit Settings Menu")
              (list (lambda () (if (eq (type-of *mode*) 'title-mode)
                              (toggle-settings-menu)
                              (progn
                                (toggle-settings-menu nil)
                                (switch-mode 'title-mode))))
                    (lambda () (toggle-mode-music *mode*))
                    (lambda () (format t "quitting...~%")
                       (tgk:stop))
                    (lambda () (toggle-settings-menu)))
            :name :settings-menu
            :origin (tgk:vec2 480 300)
            :width 320
            :height 180
            :font-size font-size
            :background-color menu-color
            :visibility :active))))

(defun draw-settings-menu ()
  (draw-menu (root-menu *mode*)))


;;; controls menu
(let ((previous-menu nil))
 (defun toggle-controls-menu (&optional (new-root-menu previous-menu))
   (print "Toggled Settings Menu")
   (if *show-controls-menu* ; we're leaving the controls menu
       (progn (unbind-buttons *mode*)
              (bind-buttons *mode*)
              (setf (root-menu *mode*)
                    (setf (active-menu *mode*) new-root-menu))
              (setf previous-menu nil))
       (progn (unbind-buttons *mode*)
              (bind-controls-buttons *mode*)
              (setf previous-menu (root-menu *mode*))
              (setf (root-menu *mode*)
                    (setf (active-menu *mode*) *controls-menu*))))
   (setf *show-controls-menu*
         (not *show-controls-menu*))))

(defun bind-controls-buttons (mode)
  (setf (bound-buttons mode)
        (list :escape ))
  (with-bind-buttons ()
    ((:escape :pressed) (toggle-controls-menu))))

(defun create-controls-menu ()
  (let ((font-size 40)
        (menu-color (alpha-color .8 (hexcolor "#110501"))))
    (setf *controls-menu*
          (make-instance 'info-menu
                         :text (list
                                "Move right - Right Arrow, l"
                                "Move left  - Left Arrow, h"
                                "Move up    - Up Arrow, j"
                                "Move down  - Down Arrow, k"
                                ""
                                "Select Menu Item - "
                                "         Right Arrow, Enter, l"
                                ""
                                "Open Inventory - e"
                                "Open stats - Tab"
                                ""
                                "Open Settings - Esc")
                         :name :controls-menu
                         :origin (tgk:vec2 400 100)
                         :width 460
                         :height 520
                         :font :quikhand
                         :font-size font-size
                         :background-color menu-color
                         :visibility :active))))

(defun draw-controls-menu ()
  (draw-menu (root-menu *mode*)))
